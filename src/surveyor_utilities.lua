if SURV_UTIL then
	-- only include once
	return nil
end

SURV_UTIL = {};

----------------------------------------------------------
-- UTILITY FUNCTIONS
-- -------------------------------------------------------

SURV_UTIL.round = function(number, decimal)
	local multiplier = 10^(decimal or 0)
	return math.floor(number * multiplier + 0.5) / multiplier
end

----------------------------------------------------------
SURV_UTIL.IsInsideCircle = function(x_center, y_center, radius, checkX, checkY)

	local distance = (math.sqrt(math.pow((checkX - x_center), 2) + math.pow((checkY - y_center), 2))) + 2
	--log("Distance: "..tostring(distance).." and Radius: "..radius);
	
	if radius == 0 then
		return {isInCircle = true, distance = distance};
	end
	
	if distance < radius then
		return {isInCircle = true, distance = distance};
	else
		return {isInCircle = false, distance = distance};
	end
end

----------------------------------------------------------
SURV_UTIL.print_system_message = function(message)
	Component.GenerateEvent("MY_SYSTEM_MESSAGE", {text=message})
end

----------------------------------------------------------
SURV_UTIL.SendMarkerMessage = function(input)
	--log("Send Marker Message "..tostring(input.type).." - "..tostring(input.data));
	Component.PostMessage("WorldMap:Main", input.type, tostring(input.data));
end

----------------------------------------------------------
SURV_UTIL.GetMONMonth = function(month)
	local months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	return months[tonumber(month)];
end

----------------------------------------------------------
SURV_UTIL.ParseDate = function(YMD)
	--local century = string.sub(YMD,1,2);
	local yearYY = string.sub(YMD,3,4);
	local yearCCYY = string.sub(YMD,1,4);
	local month = string.sub(YMD,5,6);
	local monthMON = SURV_UTIL.GetMONMonth(month);
	local day = string.sub(YMD,7,8)
	
	return {yearYY = tonumber(yearYY), yearCCYY = tonumber(yearCCYY), month = tonumber(month), monthMON = monthMON, day = tonumber(day)};
end

----------------------------------------------------------
SURV_UTIL.ParseTime = function(HHMM)
	local hours = string.sub(HHMM,1,2);
	local minutes = string.sub(HHMM,3,4);
	
	return {hours = hours, minutes = minutes};
end

----------------------------------------------------------
SURV_UTIL.FormatDate = function(parsedDate, dateFormat)
	local datevalue = dateFormat;
	--log("Converting date "..tostring(parsedDate).." to format "..dateFormat);
	datevalue = string.gsub(datevalue, "DD", parsedDate.day);
	datevalue = string.gsub(datevalue, "CCYY", parsedDate.yearCCYY);
	datevalue = string.gsub(datevalue, "YY", parsedDate.yearYY);
	datevalue = string.gsub(datevalue, "MM", parsedDate.month);
	datevalue = string.gsub(datevalue, "MON", parsedDate.monthMON);
	
	return datevalue;
end

----------------------------------------------------------
SURV_UTIL.AllowedCheck = function(userSetting)
	local CharName = Player.GetInfo();
	local AsshatList = {

	}
	
	for i, name in pairs(AsshatList) do
		if CharName == name then
			userSetting = false;
		end
	end
	
	return userSetting;
end

----------------------------------------------------------
SURV_UTIL.DateFindHoursDiff = function(startDate, endDate, startTime, endTime)
	--time values are optional
	--we are not attempting an accurate determination of hours
	--we're just getting an estimate and making sure we handle
	--for end of year date crossover
	
	--Debug.Log("Date Find Hours Diff");
	local startDateDetails = {};
	local endDateDetails = {};
	local startTimeDetails = {};
	local endTimeDetails = {};
	local totalHours = 0;
	
	if startDate ~= nil then
		startDateDetails = SURV_UTIL.ParseDate(startDate);
		--Debug.Table("   Start date details", startDateDetails);
	else
		Debug.Log("   DateFindHoursDiff - startDate not defined.")
	end
	if endDate ~= nil then
		endDateDetails = SURV_UTIL.ParseDate(endDate);
		--Debug.Table("   End date details", endDateDetails);
	else
		Debug.Log("   DateFindHoursDiff - endDate not defined.")
	end
	if startTime ~= nil then
		startTimeDetails = SURV_UTIL.ParseTime(startTime);
		--Debug.Table("Start time details", startTimeDetails);
	end
	if endTime ~= nil then
		 endTimeDetails = SURV_UTIL.ParseTime(endTime);
		 --Debug.Table("   End time details", endTimeDetails);
	end
	
	--if we're crossing a year boundary then we need to add 12 to our end month for each year boundary
	if endDateDetails.yearCCYY > startDateDetails.yearCCYY then
		--Debug.Log("   Year difference", startDateDetails.yearCCYY, endDateDetails.yearCCYY);
		endDateDetails.month = endDateDetails.month + ((endDateDetails.yearCCYY - startDateDetails.yearCCYY) * 12);
	end
	
	--if we're crossing a month boundary then we are adding 31 days for each month crossed. Remember, this is just an estimate.
	if endDateDetails.month > startDateDetails.month then
		--Debug.Log("   Month difference", startDateDetails.month, endDateDetails.month);
		endDateDetails.day = endDateDetails.day + ((endDateDetails.month - startDateDetails.month) * 31); 	
	end
	
	--finally, if we're crossing days then we add 24 hours per day
	if endDateDetails.day > startDateDetails.day then
		--Debug.Log("   Day difference", startDateDetails.day, endDateDetails.day);
		endTimeDetails.hours = endTimeDetails.hours + ((endDateDetails.day - startDateDetails.day) * 24);
	end
	
	--Debug.Log("   Hours difference", startTimeDetails.hours, endTimeDetails.hours);
	local totalHours = endTimeDetails.hours - startTimeDetails.hours;
	
	return tonumber(totalHours);
end