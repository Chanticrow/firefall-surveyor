--[[
Marker functionality should only need to create, delete, and show.
No plans to update in place.  Updates are destroy the old one and build a new one.
"surveyor_marker" library will manage the marker collection.
It will pass a marker into this sub-library to be actioned.
]]

if SURV_SUBMARKER then
	-- only include once
	return nil
end

SURV_SUBMARKER = {};

local INTERFACE_VARS = {};

require "lib/lib_MapMarker";


----------------------------------------------------------
SURV_SUBMARKER.SetInterfaceValues = function(inINTERFACE_VARS)
	INTERFACE_VARS = inINTERFACE_VARS;
end

----------------------------------------------------------
SURV_SUBMARKER.RemoveBits = function(thisMarker)
	log("   Classic marker - attempting to remove diamond.")
	
	MapMarker.QueryMarkers(SURV_SUBMARKER.HandleQuery);
end

----------------------------------------------------------
SURV_SUBMARKER.HandleQuery = function(markerList)
	log("   Classic marker - handling marker query.")
	
	--Debug.Table(markerList);
	
	for i, markerData in pairs(markerList) do
		if string.sub(markerData.markerId, 1, 8) == "surveyor" then
			
		end 
	end
end

----------------------------------------------------------
SURV_SUBMARKER.BuildMarker = function(thisMarker)
	--thisMarker is a generic structure to define the inputs for a marker.
	Debug.Table("Creating classic marker", thisMarker);

	local markerId = thisMarker.markerId;
	local pos = {x=thisMarker.x, y=thisMarker.y, z=thisMarker.z};
	local titleText = "";
	local bodyText = "";
	
	for i, resource in pairs(thisMarker.resources) do
		titleText = titleText..resource.display.."\n";
		bodyText = bodyText..resource.name.." "..resource.resource_quality.." - "..(resource.percent * 100).."\n";
	end
	
	local myMarker = MapMarker.Create(markerId);
	myMarker:SetTitle(titleText)
	myMarker:SetBodyText(bodyText)
	myMarker:BindToPosition(pos)
	
	if INTERFACE_VARS.interface_WebIcons == true then
		myMarker:GetIcon():SetUrl(tostring(thisMarker.web_icon));
		myMarker:GetIcon():SetParam("scaleX", 0.5);
		myMarker:GetIcon():SetParam("scaleY", 0.5);
		myMarker:GetIcon():SetParam("glow", thisMarker.qualityColor);
	else
		myMarker:GetIcon():SetTexture("icons", "crystite")
		myMarker:GetIcon():SetParam("tint", thisMarker.qualityColor)
	end
	
	myMarker:GetIcon():SetParam("alpha", thisMarker.alpha);
	myMarker:SetThemeColor(thisMarker.qualityColor)  --Sets the title color	

	SURV_SUBMARKER.RemoveBits(thisMarker);

	return myMarker;
end

----------------------------------------------------------
SURV_SUBMARKER.ShowMarker = function(myMarker, worldmapFlag, radarFlag, hudFlag)

	--Debug.Table("MarkerShow: ",myMarker);
	--Debug.Log("Show flags: ", worldmapFlag, radarFlag, hudFlag);
	
	myMarker:ShowOnWorldMap(worldmapFlag);
	myMarker:ShowOnRadar(radarFlag);
	myMarker:ShowOnHud(hudFlag);

end

----------------------------------------------------------
SURV_SUBMARKER.GetPosition = function(myMarker)
	--expected return is one object with x, y, and z coordinates in it
	return myMarker.pos;
end

----------------------------------------------------------
SURV_SUBMARKER.DestroyMarker = function(myMarker)
	--Debug.Table("Destroying Marker", myMarker);
	myMarker:Destroy();
end

