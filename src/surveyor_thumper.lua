if SURV_THUMP then
	-- only include once
	return nil
end

SURV_THUMP = {};

local listSquad = {};
local listThumpers = {};
local isThumping = false;

----------------------------------------------------------
-- THUMPER FUNCTIONS
-- -------------------------------------------------------
 local function IsThisMyThumper(args)

	--log("Is This My Thumper: "..tostring(args))
	--log("Squad: "..tostring(listSquad))
	local thisThumper = {entityId = tostring(args.entityId)}
	thisThumper.info = Game.GetTargetInfo(args.entityId);
	
	if listThumpers[thisThumper.entityId] == nil then
	
		if thisThumper.info == nil then
			return false;  --someone lost a thumper, but it wasn't ours
		end
		
		for _,MemberID in pairs(listSquad) do
			if (MemberID == thisThumper.info.ownerId) then
				return true;  --
			end
		end
	else
		return true;  --thumper is already in my list
	end

	return false;
end

----------------------------------------------------------
local function TrackThumper(args)
	local entityId = tostring(args.entityId);
	
	listThumpers[entityId] = 1;
	isThumping = true;
	SURV_MARKER.ShowHideToggle(true);
end

----------------------------------------------------------
local function ReleaseThumper(args)
	local entityId = tostring(args.entityId);
	
	if listThumpers[entityId] ~= nil then
		listThumpers[entityId] = nil;
		isThumping = false;
		SURV_MARKER.ShowHideToggle(false);
	end
end

----------------------------------------------------------
SURV_THUMP.ManageThumper = function(args, interface_ThumpingHide)
	--Debug.Table("  Manage Thumper", args);
	local myThumper = false;
	
	if interface_ThumpingHide == true then
		if isThumping == false then
			if (args.type == "resourcenode") then
				myThumper = IsThisMyThumper(args);
				
				if myThumper == true then
					TrackThumper(args);
				end
			end
		else
			ReleaseThumper(args);
		end
	end
end

----------------------------------------------------------
SURV_THUMP.UpdateSquad = function()
	local roster = Squad.GetRoster();
	
	if (roster) then
		if (#roster.members ~= #listSquad) then

			listSquad = {}
			for _,Member in pairs(roster.members) do
				table.insert(listSquad, Member.entityId)
			end
		end
	else
		listSquad = {Player.GetTargetId()}
	end
end