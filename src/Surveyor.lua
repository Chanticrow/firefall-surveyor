--- Surveyor - The Scan Hammer Tracker.
-- @author Chanticrow
-- @release 0.20

-- With sincere gratitude to Hanachi, LemonKing, livervoids, RadthorDax, and Ced23Ric for all the support, inspiration,
--   and code examples.  

----------------------------------------------------------
-- Changelog

-- 0.1
--	Initial Release

-- 0.2 
--  Show/Hide markers
--  Remove markers
--  Clear all data function

-- 0.3
--  Clear area around this node 
--  Icon and text colored by quality
--  Bug fixes

-- 0.4
--  Interface option to use quality colors
--  Interface option to track squad scans
--  Enable/disable addon functionality

-- 0.5
--  Proximity checking
--  Mineral filtering
--  "Keep best scan" filter added

-- 0.6
--  Refactored show/hide code for efficiency and flicker removal.
--  Proximity checking fixes

-- 0.7
--  Bug fixes

-- 0.8 2012/12/20
--  Changed quality color lookup to use R5 lib_Items instead of homegrown lookup.
--  Added localization logic.
--  Added German language support - thanks, Manni1024
--  Fixed double marker load on start up.
--  Added dynamic keep best scan functionality.
--  Added auto show/hide when thumping.
--  Added option to show web icons instead of flat icons.
--  Fixed a data wipe issue that required an /rui to clear scans.

-- 0.9 - 2013/01/20
--  Added option to hide default scan marker.
--  Added option to hide Surveyor markers by default.
--  Added option to automatically remove markers after a set number of days.
--  Added Polish translation - thanks, Drake84pl!
--  Fixed data storage issue causing the marker count to show as zero at startup.
--  Added mineral qualities to translations, and fixed some other translation items.  Thanks, Drake84pl.

-- 0.10a
--  Special edition for tracking mineral types over time.

-- 0.11 - 2013/03/22
--  Updated for milestone patch .6
--  Added mineral type tracking.  Use "/surv list" to see minerals.

-- 0.12 - 2013/04/20
--  Fixed for new scan billboards.
--  Added date format option in interface.
--  Added three new family and constraint marker formats in interface.
--  Added slash command for clearing all markers in X radius. Defaults to clear radius from interface.

-- 0.13 - 2013/04/21
--  Fixed callback spamming when recreating markers.

-- 0.14 - 2013/04/22
--  Added French translation - thanks, NOTHAN!

-- 0.15 - 2013/05/04
--  Added minimum quality setting (Rhef/Rhef)

-- 0.16 - 2013/05/24
--  Added new Minerals cleaned up old (Cryptis Midnight)

-- 0.17 - 2013/06/03
--  Integrated 0.15 and 0.16 into versioned build.
--  Corrected Family, Group and other mineral issues for marker formatting options.
--  Added translations for Quality Filter from 0.15
--  Added show/hide all options to mineral filter
--  Fixed some ordering issues with mineral filter
--  Added percentage filtering

-- 0.18 - 2013/06/07
--  Fixed a duplicate entry in the mineral lookup table.
--  Updated Melder info for 0.6.1641

-- 0.19 - 2013/07/01
--  Updated for patch 0.7.1665
--  Fixed Biopolymer misspelling
--  Added Melded Crystite
--  Uses new coordinate chunking system.

-- 0.20
--  Complete refactor of addon.
--]]

----------------------------------------------------------
-- Included libraries.
-- -------------------------------------------------------
-- Need the navwheel for menuing.
-- Need the map marker for creating markers
require "math";
require "string";
require "table";
require "lib/lib_NavWheel";
require "lib/lib_InterfaceOptions"
require "lib/lib_Slash";
require "lib/lib_Items";  
require "lib/lib_table";
require "lib/lib_Callback2";
require "lib/lib_Debug";
require "./Localization/localization";
require "./lib/Fing";
require "./surveyor_utilities";
require "./surveyor_resources";
require "./surveyor_marker";
require "./surveyor_thumper";

---------------------------------------------------------- 
-- Constants
local const_Abil_ScanHammer = "34503";


---------------------------------------------------------- 
-- Variableness
local surveyorEnabled = true;
local surveyorStartupDelay = 10;  --seconds
local storedData = {};
local storedScans = {};
local loadedAndReady = false;
local scanCount = 0;
local mineralFilterOn = false;
local mineralFilterTable = {};
local comparisonListHold = {};
local l_DebugFlag = true;

local CONST_NavWheelColor = "#0099FF";

local USEFUL_VALUES = {
	 l_instanceId = 0
	,l_playerId = 0
	,l_zoneId = 0
	,l_pvpFlag = false
};

local INTERFACE_VARS = {
	 interface_ClearRadius = 40  
	,interface_ColorByQuality = true
    ,interface_TitleFormat = "FULL_NAME_FMT" 
    ,interface_MarkerStyle = "CLASSIC"    				
    ,interface_RecordSquadScans = true 
    ,interface_FadeWithDistance = true 
    ,interface_FadeDistance = 500 
    ,interface_bestScanOn = true 
    ,interface_bestScanQty = 3 
    ,interface_bestScanDistance = 100 
    ,interface_ThumpingHide = true 
    ,interface_WebIcons = false 
    ,interface_WebIconsSort = "WEBICONS_QUALITY"
    ,interface_HideDefaultMarker = true 
    ,interface_DefaultMarkerFadeTime = 4 
    ,interface_HideByDefault = false 
    ,interface_AutoAgeScans = true 
    ,interface_ScanAge = 48
    ,ScanAge_Half = 24
    ,interface_DateFormat = "CCYY-MON-DD" 
    ,interface_MinQuality = 1 
    ,interface_MinPercent = 1 
    ,interface_AnnounceFailures = true
    ,interface_DetectResourceChanges = true
    ,interface_DetectResourceChangesDistance = 30
    ,interface_ShowTotalPercent = true
    ,interface_DisplayStartupMessage = true
    ,ScaleRampA = 10
    ,ScaleRampB = 100
    ,ScaleRampC = .3
    ,ScaleRampD = 25
};

local localization_Settings = "Addon_Surveyor_Localization";

local versionSurveyor = "0.19";
local upgradeSurveyor = {};  --manage version and upgrades

----------------------------------------------------------

local mineralTypeIds = {
	 ["Raw Copper^CY"]=78014
	,["Raw Iron^CY"]=78015
	,["Raw Aluminum^CY"]=78016
	,["Raw Carbon^CY"]=78017
	,["Raw Silicate^CY"]=78018
	,["Raw Ceramics^CY"]=78019
	,["Raw Methine^CY"]=78020
	,["Raw Octine^CY"]=78021
	,["Raw Nitrine^CY"]=78022
	,["Raw Radine^CY"]=82420
	,["Raw Petrochemical^CY"]=78023
	,["Raw Biopolymer^CY"]=78024
	,["Raw XenoGrafts^CY"]=78025
	,["Raw Toxins^CY"]=78026
	,["Raw Regenics^CY"]=78027
	,["Raw Anabolics^CY"]=78028
	,["Melded Crystite"]=82628
};

local mineralFamilies = nil;
local mineralConstraints = nil;
local mineralGroups = nil;

--built on component load
local mineralLookupTable = {};

----------------------------------------------------------
-- Slash Doodads
local SURV_SLASH_CMDS = {};

---------------------------------------------------------- 
-- NavWheel Bits
local NODES = {};
local MAP_ROOT_NODE_ID = "map_root";
local MAIN_ROOT_NODE_ID = "map_root";

----------------------------------------------------------
local function ReadyCheck()
	if loadedAndReady ~= true then
		return false;
	else
		return true;
	end
end

----------------------------------------------------------
local function PvPCheck()
	return USEFUL_VALUES.l_pvpFlag;
end

----------------------------------------------------------
local function SetEnable(enableFlag)
	surveyorEnabled = enableFlag;
	--surveyorEnabled = SURV_UTIL.AllowedCheck(enableFlag);
end

----------------------------------------------------------
local function SetInterfaceValues(calledFrom)
	Debug.Log("Setting Interface Values from ", calledFrom);
	Debug.Table("Main setting interface values ", INTERFACE_VARS);
	
	SURV_RES.SetInterfaceValues(USEFUL_VALUES, INTERFACE_VARS);
	
	SURV_MARKER.SetInterfaceValues(USEFUL_VALUES, INTERFACE_VARS, mineralFilterTable);
end


----------------------------------------------------------
-- SCAN & RESOURCE LOGIC
-- -------------------------------------------------------

----------------------------------------------------------
local function ConstructAllMarkers()

	SURV_MARKER.DeleteAllMarkers();
	
	local scanList = SURV_RES.GetListOfDistinctScans();
	
	for scanId, count in pairs(scanList) do
		SURV_MARKER.ProcessMarker(scanId);
		scanCount = scanCount + 1;
	end
	
end


----------------------------------------------------------
local function OptChkSurveyorEnabled()
	local result = true;
	if surveyorEnabled ~= true then
		result = false;
	else
		result = true;
	end
	
	--Debug.Log("   Option Check: Surveyor Enabled: ", result);
	return result;
end

----------------------------------------------------------
local function OptChkHideDefaultMarker(scanId)
	if INTERFACE_VARS.interface_HideDefaultMarker == true then
		Callback2.FireAndForget(function() Game.AcceptResourceScan(scanId, false); end, nil, INTERFACE_VARS.interface_DefaultMarkerFadeTime)
	end
end

----------------------------------------------------------
local function OptChkRecordSquadScans(args)
	--One more check to make sure it's ours
	local result = true;
	if INTERFACE_VARS.interface_RecordSquadScans == false then
		if args.ownerId ~= Player.GetTargetId() then
			result = false;
		else
			result = true;
		end
	else
		result = true;
	end
	
	--Debug.Log("   Option Check: Record Squad Scans: ", result);
	return result;
end

----------------------------------------------------------
local function ValidateScan(args)
	--Debug.Log("Validate Scan");
	local processThisScan = true;
	
	processThisScan = OptChkSurveyorEnabled();
	
	if processThisScan == true then
		processThisScan = ReadyCheck();
		--Debug.Log("   Validation ready check: ", processThisScan)
	end
	
	if processThisScan == true then
		processThisScan = OptChkRecordSquadScans(args);
	end
	
	--Debug.Log("Validate proved ", processThisScan);
	return processThisScan;	
end		

----------------------------------------------------------
local function ScanFailedHandler(args)
	if args["nearby_thumper"] ~= nil and args["nearby_thumper"] == true then
		log("Scan Results: Too close to another thumper.");
	end
	
	if args["invalid_surface"] ~= nil and args["invalid_surface"] == true then
		log("Scan Results: Invalid surface.");
	end
	
	if args["empty"] == true then
		log("Scan Results: Trace resources.");
		SURV_RES.CleanFailedArea(args);
	end
end

----------------------------------------------------------
-- NAVWHEEL OPTIONS
-- -------------------------------------------------------

local function Surveyor_MainScreen_NavWheel_Init()

	NODES.SURVEYOR_OPTIONSNODE = NavWheel.CreateNode("SURVEYOR_OPTIONSNODE");
	NODES.SURVEYOR_OPTIONSNODE:GetIcon():SetTexture("icons", "crystite");
	NODES.SURVEYOR_OPTIONSNODE:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_OPTIONSNODE:SetDescription("Surveyor");
	NODES.SURVEYOR_OPTIONSNODE:SetParent(MAIN_ROOT_NODE_ID, -5);
	
	NODES.SURVEYOR_SHOWMARKERS = NavWheel.CreateNode("SURVEYOR_SHOWMARKERS");
	NODES.SURVEYOR_SHOWMARKERS:GetIcon():SetTexture("icons", "waypoint");
	NODES.SURVEYOR_SHOWMARKERS:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_SHOWMARKERS:SetDescription(Get_Localization_Value("NODE_SURVEYOR_SHOWMARKERS"));
	NODES.SURVEYOR_SHOWMARKERS:SetAction(function()
			SURV_MARKER.ShowHideToggle();
			NavWheel.Close();
		end);
		
	--[[
	NODES.SURVEYOR_KEEPBEST = NavWheel.CreateNode("SURVEYOR_KEEPBEST");
	NODES.SURVEYOR_KEEPBEST:GetIcon():SetTexture("icons", "raw_resource");
	NODES.SURVEYOR_KEEPBEST:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_KEEPBEST:SetDescription(Get_Localization_Value("NODE_SURVEYOR_KEEPBEST"));
	NODES.SURVEYOR_KEEPBEST:SetAction(function()
			KeepBestScans(scanList);
			MarkersInitialize();
			NavWheel.Close();
		end);		
	]]
		
	NODES.SURVEYOR_DELALLMARKERS = NavWheel.CreateNode("SURVEYOR_DELALLMARKERS");
	NODES.SURVEYOR_DELALLMARKERS:GetIcon():SetTexture("icons", "no");
	NODES.SURVEYOR_DELALLMARKERS:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_DELALLMARKERS:SetDescription(Get_Localization_Value("NODE_SURVEYOR_DELALLMARKERS"));
	
	NODES.SURVEYOR_DELALLMARKERSYES = NavWheel.CreateNode("SURVEYOR_DELALLMARKERSYES");
	NODES.SURVEYOR_DELALLMARKERSYES:GetIcon():SetTexture("icons", "no");
	NODES.SURVEYOR_DELALLMARKERSYES:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_DELALLMARKERSYES:SetDescription(Get_Localization_Value("NODE_SURVEYOR_DELALLMARKERSYES"));
	NODES.SURVEYOR_DELALLMARKERSYES:SetParent("SURVEYOR_DELALLMARKERS", 1);
	NODES.SURVEYOR_DELALLMARKERSYES:SetAction(function()
		SURV_RES.DeleteAllScans();
		ConstructAllMarkers();
		NavWheel.Close();
	end);		
	
	NODES.SURVEYOR_DELALLMARKERSYES2 = NavWheel.CreateNode("SURVEYOR_DELALLMARKERSYES2");
	NODES.SURVEYOR_DELALLMARKERSYES2:GetIcon():SetTexture("icons", "no");
	NODES.SURVEYOR_DELALLMARKERSYES2:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_DELALLMARKERSYES2:SetDescription(Get_Localization_Value("NODE_SURVEYOR_DELALLMARKERSYES2"));
	NODES.SURVEYOR_DELALLMARKERSYES2:SetParent("SURVEYOR_DELALLMARKERS", 1);
	NODES.SURVEYOR_DELALLMARKERSYES2:SetAction(function()
		SURV_RES.DeleteAllScans(USEFUL_VALUES.l_instanceId);
		ConstructAllMarkers();
		NavWheel.Close();
	end);		
	
	NODES.SURVEYOR_DELALLMARKERSNO = NavWheel.CreateNode("SURVEYOR_DELALLMARKERSNO");
	NODES.SURVEYOR_DELALLMARKERSNO:GetIcon():SetTexture("icons", "return");
	NODES.SURVEYOR_DELALLMARKERSNO:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_DELALLMARKERSNO:SetDescription(Get_Localization_Value("NODE_SURVEYOR_DELALLMARKERSNO"));
	NODES.SURVEYOR_DELALLMARKERSNO:SetParent("SURVEYOR_DELALLMARKERS", 2);	
	NODES.SURVEYOR_DELALLMARKERSNO:SetAction(function()
			NavWheel.Close();
	end);		
	
	--MINERAL FILTERING NODES
		
	NODES.SURVEYOR_FILTERMAIN = NavWheel.CreateNode("SURVEYOR_FILTERMAIN");
	NODES.SURVEYOR_FILTERMAIN:GetIcon():SetTexture("icons", "raw_resource");
	NODES.SURVEYOR_FILTERMAIN:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_FILTERMAIN:SetDescription(Get_Localization_Value("NODE_SURVEYOR_FILTERMAIN"));	
	
	NODES.SURVEYOR_FILTER_SHOWALL = NavWheel.CreateNode("SURVEYOR_FILTER_SHOWALL");
	NODES.SURVEYOR_FILTER_SHOWALL:GetIcon():SetTexture("icons", "raw_resource");
	NODES.SURVEYOR_FILTER_SHOWALL:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_FILTER_SHOWALL:SetDescription(Get_Localization_Value("NODE_SURVEYOR_FILTER_SHOWALL"));
	NODES.SURVEYOR_FILTER_SHOWALL:SetParent("SURVEYOR_FILTERMAIN", 200);
	NODES.SURVEYOR_FILTER_SHOWALL:SetAction(function()
		for itemTypeId, itemDetails in pairs(mineralLookupTable) do
			local nodeNameOn = "SURVEYOR_"..itemTypeId.."_ON";	
			local nodeNameOff = "SURVEYOR_"..itemTypeId.."_OFF";
			mineralFilterTable[itemTypeId] = true;
			NODES[nodeNameOff]:SetParent(nil);  			--turn off this node
			NODES[nodeNameOn]:SetParent("SURVEYOR_FILTERMAIN");  --turn on the alternate node
		end
		SetInterfaceValues("Filter Showall");
	end);
	
	NODES.SURVEYOR_FILTER_SHOWNONE = NavWheel.CreateNode("SURVEYOR_FILTER_SHOWNONE");
	NODES.SURVEYOR_FILTER_SHOWNONE:GetIcon():SetTexture("icons", "raw_resource");
	NODES.SURVEYOR_FILTER_SHOWNONE:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_FILTER_SHOWNONE:SetDescription(Get_Localization_Value("NODE_SURVEYOR_FILTER_SHOWNONE"));
	NODES.SURVEYOR_FILTER_SHOWNONE:SetParent("SURVEYOR_FILTERMAIN", 100);
	NODES.SURVEYOR_FILTER_SHOWNONE:SetAction(function()
		for itemTypeId, itemDetails in pairs(mineralLookupTable) do
			local nodeNameOn = "SURVEYOR_"..itemTypeId.."_ON";	
			local nodeNameOff = "SURVEYOR_"..itemTypeId.."_OFF";
			mineralFilterTable[itemTypeId] = false;
			NODES[nodeNameOn]:SetParent(nil);  			--turn off this node
			NODES[nodeNameOff]:SetParent("SURVEYOR_FILTERMAIN");  --turn on the alternate node
		end
		SetInterfaceValues("Filter Shownone");
	end);	
	
	local orderOn = 20;
	local orderOff = 10;
	
	for itemTypeId, itemDetails in pairs(mineralLookupTable) do
	
		local nodeNameOn = "SURVEYOR_"..itemTypeId.."_ON";	
		local nodeNameOff = "SURVEYOR_"..itemTypeId.."_OFF";	
		
		--log("Building filter nodes "..nodeNameOn.." and "..nodeNameOff.." for item "..itemDetails.shortname.." "..itemTypeId)
			
		NODES[nodeNameOn] = NavWheel.CreateNode(nodeNameOn);
		NODES[nodeNameOn]:GetIcon():SetUrl(itemDetails.web_icon);
		NODES[nodeNameOn]:GetIcon():SetParam("scaleX", 0.5);
		NODES[nodeNameOn]:GetIcon():SetParam("scaleY", 0.5);
		NODES[nodeNameOn]:GetIcon():SetParam("alpha", 1);
		NODES[nodeNameOn]:GetIcon():SetParam("glow", "#CCCCCC");
		NODES[nodeNameOn]:SetDescription(itemDetails.shortname);
		NODES[nodeNameOn]:SetParent("SURVEYOR_FILTERMAIN", orderOn);  --default filters to be on
		NODES[nodeNameOn]:SetAction(function()
			--log('Turning off mineral '..itemTypeId..' '..itemDetails.shortname..' which is node names '..nodeNameOn..' and '..nodeNameOff);
			mineralFilterTable[itemTypeId] = false;  	--if we clicked this we're turning off this mineral
			NODES[nodeNameOn]:SetParent(nil);  			--turn off this node
			NODES[nodeNameOff]:SetParent("SURVEYOR_FILTERMAIN", orderOff);  --turn on the alternate node
			SetInterfaceValues("Nodes Name On");
		end);		
			
		NODES[nodeNameOff] = NavWheel.CreateNode(nodeNameOff);
		NODES[nodeNameOff]:GetIcon():SetUrl(itemDetails.web_icon);
		NODES[nodeNameOff]:GetIcon():SetParam("scaleX", 0.5);
		NODES[nodeNameOff]:GetIcon():SetParam("scaleY", 0.5);
		NODES[nodeNameOff]:GetIcon():SetParam("alpha", 0.5);
		NODES[nodeNameOff]:SetDescription(itemDetails.shortname);	
		NODES[nodeNameOff]:SetAction(function()
			--log('Turning on mineral '..itemTypeId..' '..itemDetails.shortname..' which is node names '..nodeNameOn..' and '..nodeNameOff);
			mineralFilterTable[itemTypeId] = true;  	--if we clicked this we're turning on this mineral
			NODES[nodeNameOff]:SetParent(nil);  			--turn off this node
			NODES[nodeNameOn]:SetParent("SURVEYOR_FILTERMAIN", orderOn);  --turn on the alternate node
			SetInterfaceValues("Nodes Name Off");
		end);			
	
	end
		
end 

local function Surveyor_WorldMap_NavWheel_Init()
	
	NODES.SURVEYOR_REMOVE = NavWheel.CreateNode("SURVEYOR_REMOVE");
	NODES.SURVEYOR_REMOVE:GetIcon():SetTexture("icons", "no");
	NODES.SURVEYOR_REMOVE:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_REMOVE:SetDescription(Get_Localization_Value("NODE_SURVEYOR_REMOVE"));
	
	NODES.SURVEYOR_CLEANAREA = NavWheel.CreateNode("SURVEYOR_CLEANAREA");
	NODES.SURVEYOR_CLEANAREA:GetIcon():SetTexture("icons", "no");
	NODES.SURVEYOR_CLEANAREA:GetIcon():SetParam("tint", CONST_NavWheelColor);
	NODES.SURVEYOR_CLEANAREA:SetDescription(Get_Localization_Value("NODE_SURVEYOR_CLEANAREA_1")..INTERFACE_VARS.interface_ClearRadius..Get_Localization_Value("NODE_SURVEYOR_CLEANAREA_2"));
end

----------------------------------------------------------
-- INTERFACE HANDLING
-- -------------------------------------------------------

---------------------------------------------------------- 
local function Build_Interface_Options()
	
	log("Marker Style - Starting Build Interface Options: "..INTERFACE_VARS.interface_MarkerStyle);
	InterfaceOptions.StartGroup({id="SURVEYOR_MARKERSELECTION_GROUP", label=Get_Localization_Value("INTERFACE_MARKERSELECTION_GROUP")});
		InterfaceOptions.AddChoiceMenu({id="MARKERSELECTION", label=Get_Localization_Value("INTERFACE_MARKERSELECTION"), default="CLASSIC"})
			InterfaceOptions.AddChoiceEntry({menuId="MARKERSELECTION", val="CLASSIC", label=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")})
			InterfaceOptions.AddChoiceEntry({menuId="MARKERSELECTION", val="CHANTICROW", label=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CHANTICROW")})
	InterfaceOptions.StopGroup();
	log("Marker Style - Ending Build Interface Options: "..INTERFACE_VARS.interface_MarkerStyle);
	
	InterfaceOptions.StartGroup({id="SURVEYOR_DISPLAYOPTIONS", label=Get_Localization_Value("INTERFACE_DISPLAYOPTIONS")});
		InterfaceOptions.AddCheckBox({id="THUMPINGHIDE", label=Get_Localization_Value("INTERFACE_THUMPINGHIDE"), default=true});
		InterfaceOptions.AddChoiceMenu({id="TITLEFORMAT", label=Get_Localization_Value("INTERFACE_TITLEFORMAT"), default="FULL_NAME_FMT"})
			InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="FULL_NAME_FMT", label="Raw Aluminum^CY455-56%"})
			InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="SHORT_NAME_FMT", label="Alum^CY455-56%"})
		--[[	InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="ABBR_FMT", label="Alu^CY455-56%"})
			InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="COMPACT_FMT", label="Alu455/56%"})
			InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="SHORT_CONSTRAINT_FMT", label="Alu: Metal 455/56%"})
			--InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="COMPACT_CONSTRAINT_FMT", label="Alu:Min:M455/56%"})
			InterfaceOptions.AddChoiceEntry({menuId="TITLEFORMAT", val="SHORT_CONSTRAINT_FMT_2", label="56% Alu:Met 455"})
		]]
		InterfaceOptions.AddCheckBox({id="SHOWTOTALPERCENT", label=Get_Localization_Value("INTERFACE_SHOWTOTALPERCENT"), default=true});
		InterfaceOptions.AddCheckBox({id="HIDEDEFAULTMARKERS", label=Get_Localization_Value("INTERFACE_HIDEDEFAULTMARKER"), default=true});
		InterfaceOptions.AddCheckBox({id="HIDEBYDEFAULT", label=Get_Localization_Value("INTERFACE_HIDEBYDEFAULT"), default=false});
		--log("Date format during interface build: "..interface_DateFormat);
		--InterfaceOptions.AddTextInput({id="CUSTOM_DATE_FORMAT", label=Get_Localization_Value("INTERFACE_CUSTOM_DATE_FORMAT"), default=Get_Localization_Value("DATE_FORMAT"), maxlen=25, tooltip=tostring(Get_Localization_Value("INTERFACE_CUSTOM_DATE_FORMAT_TOOLTIP"))});
		InterfaceOptions.AddCheckBox({id="ANNOUNCEFAILURES", label=Get_Localization_Value("INTERFACE_ANNOUNCEFAILURES"), default=true});
		InterfaceOptions.AddCheckBox({id="DISPLAYSTARTUPMESSAGE", label=Get_Localization_Value("INTERFACE_DISPLAYSTARTUPMESSAGE"), default=true});
	InterfaceOptions.StopGroup();
	
	InterfaceOptions.StartGroup({id="SURVEYOR_MARKEROPTIONS_CLASSIC", label=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC"), subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")})
		InterfaceOptions.AddCheckBox({id="QUALITYCOLORS", label=Get_Localization_Value("INTERFACE_QUALITYCOLORS"), default=true, subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")});
		InterfaceOptions.AddCheckBox({id="WEBICONS", label=Get_Localization_Value("INTERFACE_WEBICONS"), default=false, subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")});
		InterfaceOptions.AddChoiceMenu({id="WEBICONS_SORT", label=Get_Localization_Value("INTERFACE_WEBICONS_SORT"), default="WEBICONS_QUALITY", subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")})
			InterfaceOptions.AddChoiceEntry({menuId="WEBICONS_SORT", val="WEBICONS_QUALITY", label=Get_Localization_Value("INTERFACE_WEBICONS_SORTQUALITY"), subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")})
			InterfaceOptions.AddChoiceEntry({menuId="WEBICONS_SORT", val="WEBICONS_QUANTITY", label=Get_Localization_Value("INTERFACE_WEBICONS_SORTQUANTITY"), subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")})
	InterfaceOptions.StopGroup({subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CLASSIC")});

	InterfaceOptions.StartGroup({id="SURVEYOR_MARKEROPTIONS_CHANTICROW", label=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CHANTICROW"), subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CHANTICROW")})
		InterfaceOptions.AddCheckBox({id="TEMPTHING", label="Test", default=true, subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CHANTICROW")});
	InterfaceOptions.StopGroup({subtab=Get_Localization_Value("INTERFACE_MARKEROPTIONS_CHANTICROW")});
	
	InterfaceOptions.StartGroup({id="SCALERAMP", label="Scale Ramping", subtab="ScaleRamp"})
		InterfaceOptions.AddSlider({id="SCALERAMP-A", label="Scale Ramp A", min=0, max=1000, inc=1, format="%0.0f", suffix=" m", default=10, tooltip="No idea yet.", subtab="ScaleRamp"})
		InterfaceOptions.AddSlider({id="SCALERAMP-B", label="Scale Ramp B", min=0, max=1000, inc=1, format="%0.0f", suffix=" m", default=100, tooltip="No idea yet.", subtab="ScaleRamp"})
		InterfaceOptions.AddSlider({id="SCALERAMP-C", label="Scale Ramp C", min=0, max=100, inc=.1, format="%0.0f", suffix=" m", default=.3, tooltip="No idea yet.", subtab="ScaleRamp"})
		InterfaceOptions.AddSlider({id="SCALERAMP-D", label="Scale Ramp D", min=0, max=1000, inc=1, format="%0.0f", suffix=" m", default=20, tooltip="No idea yet.", subtab="ScaleRamp"})
	InterfaceOptions.StopGroup({subtab="ScaleRamp"});
			
	InterfaceOptions.StartGroup({id="SURVEYOR_FILTEROPTIONS", label=Get_Localization_Value("INTERFACE_FILTEROPTIONS")});
		InterfaceOptions.AddSlider({id="CLEARRADIUS", label=Get_Localization_Value("INTERFACE_CLEARRADIUS"), min=1, max=150, inc=1, format="%0.0f", suffix=" m", default=40, tooltip=tostring(Get_Localization_Value("INTERFACE_CLEARRADIUS_TOOLTIP"))})
		InterfaceOptions.AddSlider({id="SHOWRADIUS", label=Get_Localization_Value("INTERFACE_SHOWRADIUS"), min=100, max=3000, inc=10, format="%0.0f", suffix=" m", default=500 })
		InterfaceOptions.AddCheckBox({id="SQUADSCANS", label=Get_Localization_Value("INTERFACE_SQUADSCANS"), default=true});
		InterfaceOptions.AddCheckBox({id="BESTSCAN", label=Get_Localization_Value("INTERFACE_BESTSCAN"), default=false, tooltip=tostring(Get_Localization_Value("INTERFACE_BESTSCAN_TOOLTIP"))});
		InterfaceOptions.AddSlider({id="BESTSCAN_THISMANY", label=Get_Localization_Value("INTERFACE_BESTSCAN_THISMANY"), min=1, max=10, inc=1, format="%0.0f", suffix=Get_Localization_Value("INTERFACE_BESTSCAN_THISMANY_SCANS"), default=3 })
		InterfaceOptions.AddSlider({id="BESTSCAN_THISDISTANCE", label=Get_Localization_Value("INTERFACE_BESTSCAN_THISDISTANCE"), min=30, max=300, inc=10, format="%0.0f", suffix=" m", default=100 })
		InterfaceOptions.AddCheckBox({id="AUTOAGESCANS", label=Get_Localization_Value("INTERFACE_AUTOAGESCANS"), default=true});
		InterfaceOptions.AddSlider({id="SCANAGE", label=Get_Localization_Value("INTERFACE_SCANAGE"), min=1, max=72, inc=1, format="%0.0f", suffix=Get_Localization_Value("INTERFACE_SCANAGE_2"), default=24 })
		InterfaceOptions.AddSlider({id="QUALITYLIMIT", label=Get_Localization_Value("INTERFACE_MIN_QUALITY_LIMIT"), min=0, max=1000, inc=1, format="^CY%d", default=1})
		InterfaceOptions.AddSlider({id="PERCENTLIMIT", label=Get_Localization_Value("INTERFACE_MIN_PERCENT_LIMIT"), min=1, max=100, inc=1, format="%d", suffix="%", default=1})
		InterfaceOptions.AddCheckBox({id="DETECTRESOURCECHANGES", label=Get_Localization_Value("INTERFACE_DETECTRESOURCECHANGES"), default=true});
		InterfaceOptions.AddSlider({id="DETECTRESOURCECHANGESDISTANCE", label=Get_Localization_Value("INTERFACE_DETECTRESOURCECHANGESDISTANCE"), min=5, max=300, inc=1, format="%0.0f", suffix=" m", default=30 })
	InterfaceOptions.StopGroup();
	
	InterfaceOptions.StartGroup({id="SURVEYOR_LOCALIZATION", label=Get_Localization_Value("INTERFACE_SURVEYOR_LOCALIZATION")});
		InterfaceOptions.AddChoiceMenu({id="LOCALIZATION", label=Get_Localization_Value("INTERFACE_LOCALIZATION"), default="English", tooltip=tostring(Get_Localization_Value("INTERFACE_LOCALIZATION_TOOLTIP"))})
		InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="English", label=Get_Localization_Value("INTERFACE_ENGLISH_LANGUAGE")})
		InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="French", label=Get_Localization_Value("INTERFACE_FRENCH_LANGUAGE")})
		InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="German", label=Get_Localization_Value("INTERFACE_GERMAN_LANGUAGE")})
		InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="Polish", label=Get_Localization_Value("INTERFACE_POLISH_LANGUAGE")})
	InterfaceOptions.StopGroup();
	
	InterfaceOptions.StartGroup({id="SURVEYOR_CREDITS", label=Get_Localization_Value("INTERFACE_CREDITS")});
		InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_1", label=Get_Localization_Value("INTERFACE_CREDITS_1"), default=false})
		InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_2", label=Get_Localization_Value("INTERFACE_CREDITS_2"), default=false})
		InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_3", label=Get_Localization_Value("INTERFACE_CREDITS_3"), default=false})
		InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_4", label=Get_Localization_Value("INTERFACE_CREDITS_4"), default=false})
		InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_5", label=Get_Localization_Value("INTERFACE_CREDITS_5"), default=false})
	InterfaceOptions.StopGroup();
end

---------------------------------------------------------- 
function OnInterfaceMessage(args)

	--Note: All of the following options are set and loaded after OnComponentLoad completes in the delayed startup area. 
	--None of these settings are loaded during OnComponentLoad.

	Debug.Table("On Interface: ", args)
	if args.type == "QUALITYCOLORS" then
		--log("   Color by Quality")
		INTERFACE_VARS.interface_ColorByQuality = (args.data == true or args.data == "true")
		if ReadyCheck() == true then
			SetInterfaceValues("Quality Colors");
			ConstructAllMarkers();
		end
	elseif args.type == "CLEARRADIUS" then
		--log("   Clear Radius")
		INTERFACE_VARS.interface_ClearRadius = args.data;		
		NODES.SURVEYOR_CLEANAREA:SetDescription(Get_Localization_Value("NODE_SURVEYOR_CLEANAREA_1")..INTERFACE_VARS.interface_ClearRadius..Get_Localization_Value("NODE_SURVEYOR_CLEANAREA_2"));
		if ReadyCheck() == true then
			SetInterfaceValues("Clear Radius");
		end
	elseif args.type == "SQUADSCANS" then
		INTERFACE_VARS.interface_RecordSquadScans = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Squad Scans");
		end
	elseif args.type == "SHOWRADIUS" then
		--log("   Show Radius")
		INTERFACE_VARS.interface_FadeDistance = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Show Radius");
			SURV_MARKER.ShowHideToggle();
			SURV_MARKER.ShowHideToggle();
		end
	elseif args.type == "TITLEFORMAT" then
		--log("   Title Format")
		INTERFACE_VARS.interface_TitleFormat = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Title Format");
			ConstructAllMarkers();
		end		
	elseif args.type == "LOCALIZATION" then
		Set_Localization_Values(args.data, localization_Settings);
		if ReadyCheck() == true then
			Build_Interface_Options("Localization");
		end
	elseif args.type == "BESTSCAN" then
		INTERFACE_VARS.interface_bestScanOn = args.data;
		if INTERFACE_VARS.interface_bestScanOn == true then
			InterfaceOptions.DisableOption("BESTSCAN_THISMANY", false)
			InterfaceOptions.DisableOption("BESTSCAN_THISDISTANCE", false)
		else
			InterfaceOptions.DisableOption("BESTSCAN_THISMANY", true)
			InterfaceOptions.DisableOption("BESTSCAN_THISDISTANCE", true)
		end
		if ReadyCheck() == true then
			SetInterfaceValues("Best Scan");
		end
	elseif args.type == "BESTSCAN_THISMANY" then
		INTERFACE_VARS.interface_bestScanQty = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Bestscan This Many");
		end
	elseif args.type == "BESTSCAN_THISDISTANCE" then
		INTERFACE_VARS.interface_bestScanDistance = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Bestscan This Distance");
		end
	elseif args.type == "THUMPINGHIDE" then
		INTERFACE_VARS.interface_ThumpingHide = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Thumping Hide");
		end
	elseif args.type == "WEBICONS" then
		INTERFACE_VARS.interface_WebIcons = args.data;
		if INTERFACE_VARS.interface_WebIcons == true then
			InterfaceOptions.DisableOption("WEBICONS_SORT", false)
		else
			InterfaceOptions.DisableOption("WEBICONS_SORT", true)
		end
		if ReadyCheck() == true then
			SetInterfaceValues("Web Icons");
			ConstructAllMarkers();
		end		
	elseif args.type == "WEBICONS_SORT" then
		INTERFACE_VARS.interface_WebIconsSort = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Web Icons Sort");
			ConstructAllMarkers();
		end	
	elseif args.type == "HIDEDEFAULTMARKERS" then
		INTERFACE_VARS.interface_HideDefaultMarker = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Hide Default Markers");
		end
	elseif args.type == "HIDEBYDEFAULT" then
		INTERFACE_VARS.interface_HideByDefault = args.data;
		if INTERFACE_VARS.interface_HideByDefault == false then
			SURV_MARKER.SetShownState(true);
		else
			SURV_MARKER.SetShownState(false);
		end
		if ReadyCheck() == true then
			SetInterfaceValues("Hide By Default");
		end
	elseif args.type == "AUTOAGESCANS" then
		INTERFACE_VARS.interface_AutoAgeScans = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Auto Age Scans");
		end
	elseif args.type == "SCANAGE" then
		INTERFACE_VARS.interface_ScanAge = args.data;
		INTERFACE_VARS.ScanAge_Half = SURV_UTIL.round(INTERFACE_VARS.interface_ScanAge/2, 0);
		if ReadyCheck() == true then
			SetInterfaceValues("Scan Age");
		end
	elseif args.type == "CUSTOM_DATE_FORMAT" then
		--log("Setting date format: "..args.data)
		INTERFACE_VARS.interface_DateFormat = args.data;
		if ReadyCheck() == true then
			Build_Interface_Options();
			SetInterfaceValues("Custome Date Format");
			ConstructAllMarkers();
		end
	elseif args.type == "QUALITYLIMIT" then
		INTERFACE_VARS.interface_MinQuality = tonumber(args.data);
		--Debug.Log("Setting minimum quality to ", args.data, " - ", INTERFACE_VARS.interface_MinQuality);
		if ReadyCheck() == true then
			SetInterfaceValues("Quality Limit");
		end
	elseif args.type == "PERCENTLIMIT" then
		INTERFACE_VARS.interface_MinPercent = tonumber(args.data);
		--Debug.Log("Setting minimum percentage to ", args.data, " - ", INTERFACE_VARS.interface_MinPercent);
		if ReadyCheck() == true then
			SetInterfaceValues("Percent Limit");
		end
	elseif args.type == "ANNOUNCEFAILURES" then
		INTERFACE_VARS.interface_AnnounceFailures = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Announce Failures");
		end
	elseif args.type == "DETECTRESOURCECHANGES" then
		INTERFACE_VARS.interface_DetectResourceChanges = args.data;
		if INTERFACE_VARS.interface_DetectResourceChanges == true then
			InterfaceOptions.DisableOption("DETECTRESOURCECHANGESDISTANCE", false)
		else
			InterfaceOptions.DisableOption("DETECTRESOURCECHANGESDISTANCE", true)
		end
		if ReadyCheck() == true then
			SetInterfaceValues("Detect Resource Changes");
		end
	elseif args.type == "DETECTRESOURCECHANGESDISTANCE" then
		INTERFACE_VARS.interface_DetectResourceChangesDistance = tonumber(args.data);
		if ReadyCheck() == true then
			SetInterfaceValues("Detect Resource Change Distance");
		end	
	elseif args.type == "SHOWTOTALPERCENT" then
		INTERFACE_VARS.interface_ShowTotalPercent = args.data;
		if ReadyCheck() == true then
			SetInterfaceValues("Show Total Percent");
			ConstructAllMarkers();
		end	
	elseif args.type == "MARKERSELECTION" then
	    log("Marker Style - Setting marker selection: "..INTERFACE_VARS.interface_MarkerStyle);
		INTERFACE_VARS.interface_MarkerStyle = args.data;
		SetInterfaceValues("Marker Selection");
		if ReadyCheck() == true then
			ConstructAllMarkers();
		end	
		if args.data == "CLASSIC" then
			Debug.Log("Classic Selected")
			InterfaceOptions.DisableOption("SURVEYOR_MARKEROPTIONS_CLASSIC", false)
			InterfaceOptions.DisableOption("SURVEYOR_MARKEROPTIONS_CHANTICROW", true)
		elseif args.data == "CHANTICROW" then
			Debug.Log("Chanticrow Selected")
			InterfaceOptions.DisableOption("SURVEYOR_MARKEROPTIONS_CLASSIC", true)
			InterfaceOptions.DisableOption("SURVEYOR_MARKEROPTIONS_CHANTICROW", false)	
		end
		log("Marker Style - Ending marker selection: "..INTERFACE_VARS.interface_MarkerStyle);
	elseif args.type == "DISPLAYSTARTUPMESSAGE" then
		INTERFACE_VARS.interface_DisplayStartupMessage = args.data;	
	elseif args.type == "SCALERAMP-A" then
		INTERFACE_VARS.ScaleRampA = args.data;
		SetInterfaceValues("Scale Ramp A");
		if ReadyCheck() == true then
			ConstructAllMarkers();
		end	
	elseif args.type == "SCALERAMP-B" then
		INTERFACE_VARS.ScaleRampB = args.data;
		SetInterfaceValues("Scale Ramp B");
		if ReadyCheck() == true then
			ConstructAllMarkers();
		end	
	elseif args.type == "SCALERAMP-C" then
		INTERFACE_VARS.ScaleRampC = args.data;
		SetInterfaceValues("Scale Ramp C");
		if ReadyCheck() == true then
			ConstructAllMarkers();
		end	
	elseif args.type == "SCALERAMP-D" then
		INTERFACE_VARS.ScaleRampD = args.data;
		SetInterfaceValues("Scale Ramp D");
		if ReadyCheck() == true then
			ConstructAllMarkers();
		end			
	end
end



----------------------------------------------------------
-- SLASH COMMANDS
-- -------------------------------------------------------
-- I totally stole a lot of the slash code from OmniTrack.  Thanks, livervoids and Hanachi!

local function InitializeSlashCommands()
	LIB_SLASH.BindCallback({slash_list = "surveyor, surv", description = "", func = SURV_SLASH_CMDS.cmdroot})
end



SURV_SLASH_CMDS.cmdroot = function(text)
	local option, message = text[1], text[2]
  
	--log("You did a surveyor slash command!"..tostring(text));
	if option ~= nil then
  		if not ( SURV_SLASH_CMDS[option] ) then log("["..option.."] Not Found") return nil end
  		SURV_SLASH_CMDS[option](message)
	else
		SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_DEFAULT"));  	
	end
end

SURV_SLASH_CMDS.help = function ()
	SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_HELP"));
end

SURV_SLASH_CMDS.show = function ()
	SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_SHOW"));
	SURV_MARKER.ShowHideToggle(false)
end

SURV_SLASH_CMDS.hide = function ()
	SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_HIDE"));
	SURV_MARKER.ShowHideToggle(true)
end

SURV_SLASH_CMDS.enable = function ()
	SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_ENABLE"));
	SetEnable(true);
end

SURV_SLASH_CMDS.disable = function ()
	SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_DISABLE"));
	SetEnable(false);
end

SURV_SLASH_CMDS.fing = function ()
	local fing_name = fing.GetInstanceName();
	SURV_UTIL.print_system_message(fing_name);
end

SURV_SLASH_CMDS.zone = function ()
	local zone_name = Game.GetZoneInfo(Game.GetZoneId()).name
	SURV_UTIL.print_system_message(zone_name);
end

SURV_SLASH_CMDS.clear = function (text)
	--log("clear in "..text.." meters.")
	SURV_UTIL.print_system_message(Get_Localization_Value("SLASH_CLEAR"))
	if text == nil then
		SURV_MARKER.CleanArea(0, INTERFACE_VARS.interface_ClearRadius, true);
	else
		SURV_MARKER.CleanArea(0, text, true);
	end
end



----------------------------------------------------------
-- START UP AND INITIALIZATION
----------------------------------------------------------

local function SetPlayerId()
	USEFUL_VALUES.l_playerId = Player.GetTargetId();
	Debug.Log("Surveyor Says: Your player id is "..tostring(USEFUL_VALUES.l_playerId));
end

----------------------------------------------------------
local function SetInstanceId()
	USEFUL_VALUES.l_instanceId = Chat.GetInstanceID();
	Debug.Log("Surveyor Says: You are now on instance "..USEFUL_VALUES.l_instanceId);
end

----------------------------------------------------------
local function SetZoneId()
	USEFUL_VALUES.l_zoneId = tostring(Game.GetZoneId());
	Debug.Log("Surveyor Says: You are now on zone "..USEFUL_VALUES.l_zoneId);
end

----------------------------------------------------------
local function SetReadyFlag()
	loadedAndReady = true;  
end

----------------------------------------------------------
local function SetPvPFlag()
	USEFUL_VALUES.l_pvpFlag = Game.IsInPvP();
	Debug.Log("Surveyor says: Your PvP flag is "..tostring(USEFUL_VALUES.l_pvpFlag));
end

----------------------------------------------------------
local function SetDebugging(arg)
	if arg == true then
		Debug.EnableLogging(true);
	else
		Debug.EnableLogging(false);
	end
	log("Debugging is enabled? "..tostring(Debug.IsLoggingEnabled()));
end

----------------------------------------------------------
local function DisplaySurveyorCount()
	local message = "";
	local fing_name = fing.GetInstanceName();
	
	--Debug.Log("Display Count", scanCount, fing_name);
	
	if scanCount == 0 then
		message = Get_Localization_Value("LOAD_MESSAGE_PART_1")..scanCount..Get_Localization_Value("LOAD_MESSAGE_PART_2c").." - "..fing_name;
	elseif scanCount == 1 then
		message = Get_Localization_Value("LOAD_MESSAGE_PART_1")..scanCount..Get_Localization_Value("LOAD_MESSAGE_PART_2a").." - "..fing_name;
	elseif scanCount > 4 then
		message = Get_Localization_Value("LOAD_MESSAGE_PART_1")..scanCount..Get_Localization_Value("LOAD_MESSAGE_PART_2c").." - "..fing_name;
	else
		message = Get_Localization_Value("LOAD_MESSAGE_PART_1")..scanCount..Get_Localization_Value("LOAD_MESSAGE_PART_2b").." - "..fing_name;
	end
	
	if INTERFACE_VARS.interface_DisplayStartupMessage == true then
		SURV_UTIL.print_system_message(message);
	end
	log(message);
end

----------------------------------------------------------
local function ProximityCheck()
	--Debug.Log("Checking proximity")
	SURV_MARKER.MarkersShowAll(); 
	Callback2.FireAndForget(ProximityCheck, nil, 2);
end

----------------------------------------------------------
local function DelayedStartItems()
	--These items need to wait for some seconds after the addon loads before executing.
	--The delay allows all data to load and be properly prepared before being acted on.
	log("Marker Style - Start DelayedStartItems: "..INTERFACE_VARS.interface_MarkerStyle);
	SetReadyFlag();
	SetInterfaceValues("Delayed Start Items");
	ConstructAllMarkers();
	DisplaySurveyorCount();
	ProximityCheck();	
	log("Marker Style - End DelayedStartItems: "..INTERFACE_VARS.interface_MarkerStyle);
end

----------------------------------------------------------
local function LoadMineralTable()
	local info = {};
	local nameCheck = nil;

	for name, itemTypeId in pairs(mineralTypeIds) do 
		info = Game.GetItemInfoByType(itemTypeId);
	
		--Debug.Table("Mineral Info ", info);
		mineralLookupTable[itemTypeId] = {};
		mineralLookupTable[itemTypeId].name = info.name;
		nameCheck = string.sub(info.name, (info.name:len() - 2), info.name:len());
		--Debug.Log("Name check ", nameCheck);
		if nameCheck == "^CY" then
			mineralLookupTable[itemTypeId].shortname = string.sub(info.name, 1, info.name:len() - 3);
		else
			mineralLookupTable[itemTypeId].shortname = info.name;
		end
		mineralLookupTable[itemTypeId].web_icon = info.web_icon;
		mineralFilterTable[itemTypeId] = true;   --Set up the mineral filters
		
	end
	
	--Debug.Table("Mineral Lookup Table", mineralLookupTable)
end

----------------------------------------------------------
-- EVENT HANDLERS
-- -------------------------------------------------------

----------------------------------------------------------
-- Initialize starting values and load the scan list from 
-- the system, build the marker list, and display the 
-- markers.
function OnComponentLoad()
	log("!!! On Component Load ")
	log("Marker Style - Start OnComponentLoad: "..INTERFACE_VARS.interface_MarkerStyle);
	loadedAndReady = false;
	
	SetDebugging(l_DebugFlag);
	
	SetEnable(true);
	
	Set_Localization_Values(nil, localization_Settings);
	
	SetPlayerId();
	
	SetInstanceId();
	
	SetZoneId();
	
	SetPvPFlag();
	
	LoadMineralTable();
	
	log("Build Interface Options")
	Build_Interface_Options();		
	
	--Handle the interface options.
	log("Interface Options Setup")
	InterfaceOptions.SetCallbackFunc(function(id, val)
		OnInterfaceMessage({type=id, data=val})
	end, "Surveyor")	
	
	-- create the slash command receiver functions
	log("Initialize Slash Commands")
	InitializeSlashCommands();	
	
	log("World Map Navwheel")
	Surveyor_WorldMap_NavWheel_Init();
	
	log("Main Screen Navwheel")
	Surveyor_MainScreen_NavWheel_Init();
	
	SURV_RES.ResourceList_Load(USEFUL_VALUES.l_instanceId);
	
	SURV_RES.AgeOutResources();
	
	--initialize the squad roster
	SURV_THUMP.UpdateSquad();
	
	log("Marker Style - Ending OnComponentLoad: "..INTERFACE_VARS.interface_MarkerStyle);
	log("!!! On Component Load Complete")
	
	Callback2.FireAndForget(DelayedStartItems, nil, surveyorStartupDelay)
end

----------------------------------------------------------
-- When the instance changes reload the scan list, the 
-- marker list, and redisplay the markers. 
function OnUpdateInstance()
	--log("!!! On Update Instance")
	
	-- checking loadedAndReady so that we only call this if the user's
	-- instance changes sometime after original login
	SetInstanceId();
	SetZoneId();
	SetPvPFlag();
	
	if PvPCheck() == true then
		SetEnable(false);
	else
		SetEnable(true);
	end
	
	if ReadyCheck() == true and OptChkSurveyorEnabled() == true then
		SetInterfaceValues("On Update Instance");
		SURV_RES.ResourceList_Load(USEFUL_VALUES.l_instanceId);
		ConstructAllMarkers();
		DisplaySurveyorCount();
	end
end

----------------------------------------------------------
function MyWaypointEvent(args)
	log("Waypoint event: "..tostring(args));
end

----------------------------------------------------------
-- When the system creates or destroys a waypoint intercept it.
-- Determine if it is a waypoint created as the result of
-- a scan hammer slam.  If so then process the scan.
-- @param args.scanId
-- @param args.command Could be "create" or "remove".
-- @param args.x
-- @param args.y
-- @param args.z
-- @param args.label The title text of the waypoint.
function OnResourceScanReport(args)
	--log("!!! My Waypoint Event")
	log("Resource Scan Report event: "..tostring(args));
	local processThisScan = false;
	
	processThisScan = ValidateScan(args);
	
	if processThisScan == true then
		local keepScan = SURV_RES.ProcessScan(args);
		
		if keepScan == true then
			SURV_MARKER.ProcessMarker(args.scanId);
			OptChkHideDefaultMarker(args.scanId);
		end
	end
end

----------------------------------------------------------
function OnResourceScanFailed(args)
	log("Resource Scan Failed event: "..tostring(args));
	ScanFailedHandler(args);
end

----------------------------------------------------------
function OnResourceScanUpdated(args)
	log("Resource Scan Updated event: "..tostring(args));
end

----------------------------------------------------------
function OnResourceScanExpired(args)
	log("Resource Scan Expired event: "..tostring(args));
end

----------------------------------------------------------
function OnPlayerReady()

	
	--ProximityCheck();
end

----------------------------------------------------------
function OnTargetAvailable(args)
	SURV_THUMP.ManageThumper(args, INTERFACE_VARS.interface_ThumpingHide);
end

----------------------------------------------------------
function OnTargetLost(args)	
	SURV_THUMP.ManageThumper(args, INTERFACE_VARS.interface_ThumpingHide);
end

----------------------------------------------------------
function OnSquadUpdate(args)
	SURV_THUMP.UpdateSquad(args);
end

----------------------------------------------------------
-- Triggers when a marker is moused over on the world map.
-- If the marker has a marker id that is in our list of 
-- currently displayed marker ids then we perform special
-- logic to off navwheel options for that marker.  
-- Otherwise we offer the standard show/hide and filtering
-- options that affect all markers.
-- @param args.markerId The id of the marker the mouse is over
function MyWorldMapSelect(args)
--log("!!! World Map Select"..tostring(args))	
	
	--we hovered on a marker id
	if args.markerId ~= nil then
		
		--if it is our marker id the show options
		local thisMarker = SURV_MARKER.GetMarker(args.markerId)
		if thisMarker ~= nil then		
			
			NODES.SURVEYOR_OPTIONSNODE:SetParent(MAP_ROOT_NODE_ID, 2000);
			NODES.SURVEYOR_REMOVE:SetParent("SURVEYOR_OPTIONSNODE", 0);	
			NODES.SURVEYOR_REMOVE:SetAction(function() 
				SURV_RES.DeleteScan(thisMarker.scanId);
				SURV_MARKER.DeleteMarker(args.markerId);
				SURV_MARKER.ShowHideToggle();
				SURV_MARKER.ShowHideToggle();
				NavWheel.Close();
			end);	
			
			NODES.SURVEYOR_CLEANAREA:SetParent("SURVEYOR_OPTIONSNODE", 0);
			NODES.SURVEYOR_CLEANAREA:SetAction(function() 
				SURV_MARKER.CleanArea(args.markerId, INTERFACE_VARS.interface_ClearRadius, false)
				NavWheel.Close();
			end);	
			
			NODES.SURVEYOR_SHOWMARKERS:SetParent(nil);
			NODES.SURVEYOR_FILTERMAIN:SetParent(nil);
			NODES.SURVEYOR_DELALLMARKERS:SetParent(nil);
			--NODES.SURVEYOR_KEEPBEST:SetParent(nil);
			
		else
			--otherwise, don't show Surveyor options on a marker that's not ours
			NODES.SURVEYOR_OPTIONSNODE:SetParent(nil);
		end
	else
		NODES.SURVEYOR_OPTIONSNODE:SetParent(MAP_ROOT_NODE_ID, -1);
		NODES.SURVEYOR_SHOWMARKERS:SetParent("SURVEYOR_OPTIONSNODE", 50);
		NODES.SURVEYOR_FILTERMAIN:SetParent("SURVEYOR_OPTIONSNODE", 45);
		NODES.SURVEYOR_DELALLMARKERS:SetParent("SURVEYOR_OPTIONSNODE", 40);
		--NODES.SURVEYOR_KEEPBEST:SetParent("SURVEYOR_OPTIONSNODE", 55);
		NODES.SURVEYOR_REMOVE:SetParent(nil);
		NODES.SURVEYOR_CLEANAREA:SetParent(nil);		
	end
end

----------------------------------------------------------
local function MapOpened()
	--log("YOU OPENED THE MAP!")
end

local function MapClosed()
	--log("YOU CLOSED THE MAP!")
end

function OnMapToggle(args)
	if args.active == true then
		MapOpened();
	else
		MapClosed();
	end
end



