--Surveyor
--German Language Resources
--0.2

local LANG_STRINGS = {
	--Interface
	INTERFACE_LOCALIZATION = "Sprache" 
	,INTERFACE_ENGLISH_LANGUAGE = "Englisch" 
	,INTERFACE_GERMAN_LANGUAGE = "Deutsch" 
	,INTERFACE_POLISH_LANGUAGE = "Polnisch"   --new 0.9
	,INTERFACE_FRENCH_LANGUAGE = "Franz�sisch"   --new 0.14
	,INTERFACE_LOCALIZATION_TOOLTIP = "Bitte nutze /rui nach dem �ndern der Sprache." 
	,INTERFACE_SURVEYOR_LOCALIZATION = "Sprache" 
	,INTERFACE_QUALITYCOLORS = "Nutze die Qualit�ts Farben" 
	,INTERFACE_SQUADSCANS = "Speichere Squad Scans" 
	,INTERFACE_CLEARRADIUS = "Radius f�r Umkreisl�schung" 
	,INTERFACE_CLEARRADIUS_TOOLTIP = ""   --new 0.9
	,INTERFACE_SHOWRADIUS = "Blende Scans aus nach" 
	,INTERFACE_TITLEFORMAT = "Anzeige Format" 
	,INTERFACE_WEBICONS = "Nutze Mineralien Icons auf der Karte" 
	,INTERFACE_SHOWDEFAULTWAYPOINT = "Zeige den Standard Scan an"
	,INTERFACE_BESTSCAN = "Automatisch die beste Scans behalten." 
	,INTERFACE_BESTSCAN_THISMANY_SCANS = " Scans"   --new 0.9
	,INTERFACE_BESTSCAN_TOOLTIP = "Jeder Scan l�scht automatisch Scans des gleichen Minerals mit weniger %."
	,INTERFACE_RECORDDATE = "Speichere Scan Datum" 
	,INTERFACE_CUSTOM_DATE_FORMAT = "Datums Format"   --new 0.12
 	,INTERFACE_CUSTOM_DATE_FORMAT_TOOLTIP = [["Optionen sind:
 	DD - Tag 
 	MM - Monat
 	MON - Monat abgek�rzt, z.B. JAN, FEB 
 	CCYY - Jahr (z.B. 2013)
 	YY - Jahr (z.B. 13)"]]  --new 0.12
 	,INTERFACE_USE_DEFAULT_SCAN_BILLBOARD = "Benutze die Standard Scan Anzeige."  --new 0.12
	
	,INTERFACE_CREDITS = "Credits" 
	,INTERFACE_CREDITS_1 = "Programmierung: Chanticrow" 
	,INTERFACE_CREDITS_2 = "Deutsche �bersetzung: Manni1024" 	
	,INTERFACE_CREDITS_3 = "Polnisch �bersetzung: Drake84pl"   --new 0.9
	,INTERFACE_CREDITS_4 = "Franz�sisch �bersetzung: Nothan"  --new 0.14
	,INTERFACE_CREDITS_5 = "Keeping Surveyor Alive: Rhef, Cryptis Midnight"  --new 0.17
	
	,INTERFACE_DISPLAYOPTIONS = "Anzeige Optionen"    --new 0.8
	,INTERFACE_FILTEROPTIONS = "Filter"    --new 0.8 
	,INTERFACE_BESTSCAN_THISMANY = "So viele beste Scans behalten: "     --new 0.8
	,INTERFACE_BESTSCAN_THISDISTANCE = "Innerhalb von: "   --new 0.8
	,INTERFACE_THUMPINGHIDE = "Verstecke Scans w�hrend du thumperst"     --new 0.8
	
	,INTERFACE_HIDEBYDEFAULT = "Verstecke Scans beim Start."   --new 0.9
	,INTERFACE_ANNOUNCEFAILURES = "Show failed scan messages"   --new 0.20
	,INTERFACE_AUTOAGESCANS = "Entferne automatisch alte Scans"  --new 0.9
	,INTERFACE_SCANAGE = "Entferne Scans �lter als"  --new 0.9
	,INTERFACE_SCANAGE_2 = "Tage"  --new 0.9
	,INTERFACE_SCANAGE_3 = "T"  --abbreviation for "days" in case it is too long to fit  --new 0.9
	,INTERFACE_POLISH_LANGUAGE = "Polnisch"   --new 0.9
	,INTERFACE_CREDITS_3 = "Polnische �bersetzung: Drake84pl"   --new 0.9
	,INTERFACE_HIDEDEFAULTMARKER = "Verstecke den Standard Scan"   --new 0.9
	
	,INTERFACE_MIN_QUALITY_LIMIT = "Verstecke niedrigere Qualit�t als:" --new 0.15
	,INTERFACE_MIN_PERCENT_LIMIT = "Verstecke weniger Prozent als:" --new 0.17
	,INTERFACE_DETECT_RESOURCE_CHANGES = "Detect resource composition changes"  --new 0.20
	,INTERFACE_SHOWTOTALPERCENT = "Show Total Resource Percent"  --new 0.20
	,INTERFACE_WEBICONS_SORT = "  Show icon of highest "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUALITY = "Quality "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUANTITY = "Quantity "  --new -0.20
	,INTERFACE_DISPLAYSTARTUPMESSAGE = "Display Surveyor count startup message"  --new 0.20
	
	--Map Nodes
	,NODE_SURVEYOR_SHOWMARKERS = "Zeige/Verstecke alle gespeicherten Scans" 
	,NODE_SURVEYOR_KEEPBEST = "Nur die besten Scans behalten" 
	,NODE_SURVEYOR_DELALLMARKERS = "L�sche alle gespeicherten Scans" 
	,NODE_SURVEYOR_DELALLMARKERSYES = "Ja, l�sche alle Scans." 
	,NODE_SURVEYOR_DELALLMARKERSYES2 = "Ja, aber nur f�r diesen Shard." 
	,NODE_SURVEYOR_DELALLMARKERSNO = "Uups, ich will doch nichts l�schen." 
	,NODE_SURVEYOR_FILTERMAIN = "Zeige Mineralien:" 
	,NODE_SURVEYOR_FILTER_SHOWALL = "Zeige alle Mineralien"   --new 0.17
	,NODE_SURVEYOR_FILTER_SHOWNONE = "verstecke alle Mineralien"   --new 0.17
	,NODE_SURVEYOR_REMOVE = "L�sche diesen Scan" 
	,NODE_SURVEYOR_CLEANAREA_1 = "L�sche Scans in " 
	,NODE_SURVEYOR_CLEANAREA_2 = "m Umkreis, au�er diesen" 
	
	--Upgrade
	,UPGRADE_NOTICE_1 = "Surveyor: Aktualisiere Surveyor auf Version " 
	,UPGRADE_NOTICE_2 = "Surveyor: Aktualisierung erfolgreich." 	
	
	--Scan Failure
	,FAILURE_QUALITY = "Scan did not meet minimum quality setting."  --new 0.20
	,FAILURE_PERCENTAGE = "Scan did not meet minimum percentage setting."  --new 0.20
	,FAILURE_BESTSCAN = "Scan resources not better than nearby scans."  --new 0.20
	
	--Slash Commands
	,SLASH_DEFAULT = 'nutze </surv help> um die Chat Befehle zu sehen.' 
	,SLASH_HELP = [["Danke, dass du Surveyor nutzt!
	Surveyor speichert die Scans auch wenn die Anzeige ausgeschaltet ist.
	</surv show> zum Anzeigen der gespeicherten Scans.
	</surv hide> zum Verstecken der gespeicherten Scans.
	</surv disable> zum Deaktivieren der Scanspeicherung.
	</surv enable> zum Aktivieren der Scanspeicherung.
	</surv list> um eine Liste der aktuellen Mineralien anzuzeigen."]]  --updated 0.12
	,SLASH_SHOW = "Surveyor sagt: Zeige Scans..." 
	,SLASH_HIDE = "Surveyor sagt: Verstecke Scans..."
	,SLASH_ENABLE = "Surveyor sagt: Scans werden gespeichert..."
	,SLASH_DISABLE = "Surveyor sagt: Scans werden nicht mehr gespeichert..."
	,SLASH_CLEAR = "Surveyor sagt: Alle Scans im Umkreis gel�scht."  --new 0.12
	
	--Miscellaneous
	,LOAD_MESSAGE_PART_1 = "Surveyor sagt: " 
	,LOAD_MESSAGE_PART_2a = " gespicherte Scan auf diesem Channel gefunden" 
	,LOAD_MESSAGE_PART_2b = " gespicherte Scans auf diesem Channel gefunden" 
	,LOAD_MESSAGE_PART_2c = " gespicherte Scans auf diesem Channel gefunden" 
	
	,WAYPOINT_BODYTEXT_1 = "Qualit�t: " 
	,MINERAL_QUALITY_SALVAGE = "Schlecht"   --new 0.9 
	,MINERAL_QUALITY_COMMON = "H�ufig"   --new 0.9
	,MINERAL_QUALITY_UNCOMMON = "Rar"   --new 0.9
	,MINERAL_QUALITY_RARE = "Selten"   --new 0.9
	,MINERAL_QUALITY_EPIC = "Episch"   --new 0.9
	,MINERAL_QUALITY_LEGENDARY = "Legend�r"   --new 0.9
	--,MINERAL_FAMILY_POLYMER = "Polymer"   --new 0.12
 	--,MINERAL_FAMILY_METAL = "Metall"   --new 0.12
 	--,MINERAL_FAMILY_CERAMIC = "Keramik"   --new 0.12
 	--,MINERAL_FAMILY_CARBON = "Kohlenstoff"   --new 0.12
 	--,MINERAL_FAMILY_BIO = "Bio"   --new 0.12
	,MINERAL_FAMILY_METAL = "Metall"  --new 0.17
	,MINERAL_FAMILY_COMPOSITE = "Verbundwerkstoffe"  --new 0.17
	,MINERAL_FAMILY_REACTIVE_GAS = "Reaktives Gas"  --new 0.17
	,MINERAL_FAMILY_INERT_GAS = "Inaktives Gas"  --new 0.17
	,MINERAL_FAMILY_BIOMATERIAL = "Biomaterial"  --new 0.17 
	,MINERAL_FAMILY_ENZYME = "Enzyme"  --new 0.17 
	,MINERAL_FAMILY_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_GROUP_MINERAL = "Mineralien"   --new 0.17 
	,MINERAL_GROUP_GAS = "Gas"   --new 0.17
	,MINERAL_GROUP_ORGANIC = "Organisch"   --new 0.17 	
	,MINERAL_GROUP_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_CONSTRAINT_POWER = Component.LookupText("STAT_POWER")   --new 0.12
	,MINERAL_CONSTRAINT_MASS = Component.LookupText("STAT_MASS")   --new 0.12
	,MINERAL_CONSTRAINT_CORES = Component.LookupText("STAT_CPU")   --new 0.12	

	,DATE_FORMAT = "DD.MM.CCYY"   --01.01.2012
	,UNKNOWN_VALUE = "Text nicht gefunden."
	};
	
function GetStrings()	
	return LANG_STRINGS;
end	