--Surveyor
--English Language Resources
--0.2

local LANG_STRINGS = {	
	INTERFACE_LOCALIZATION = "Select Language" 
	,INTERFACE_ENGLISH_LANGUAGE = "English" 
	,INTERFACE_GERMAN_LANGUAGE = "German" 
	,INTERFACE_POLISH_LANGUAGE = "Polish"   --new 0.9
	,INTERFACE_FRENCH_LANGUAGE = "French"   --new 0.14
	,INTERFACE_LOCALIZATION_TOOLTIP = "Please use /rui after changing language." 
	,INTERFACE_DISPLAYOPTIONS = "Display Options"  --new 0.8
	,INTERFACE_FILTEROPTIONS = "Filter Options"  --new 0.8
	,INTERFACE_SURVEYOR_LOCALIZATION = "Language" 
	,INTERFACE_QUALITYCOLORS = "Use Mineral Quality Colors" 
	,INTERFACE_SQUADSCANS = "Record Squad Member Scans" 
	,INTERFACE_CLEARRADIUS = "Radius for Marker Cleaning" 
	,INTERFACE_CLEARRADIUS_TOOLTIP = "Markers within this distance will be cleared."   --new 0.9
	,INTERFACE_SHOWRADIUS = "Don't Show HUD Markers Beyond" 
	,INTERFACE_TITLEFORMAT = "Marker Title Format" 
	,INTERFACE_WEBICONS = "Use Mineral Icons on Map" 
	,INTERFACE_SHOWDEFAULTWAYPOINT = "Show Default Scan Waypoint" 
	,INTERFACE_BESTSCAN = "Only Keep Best Scan While Scanning" 
	,INTERFACE_BESTSCAN_TOOLTIP = "Each scan removes previous scans of the same type with lower percentages."
	,INTERFACE_BESTSCAN_THISMANY = "  Keep this many best scans: "   --new 0.8
	,INTERFACE_BESTSCAN_THISMANY_SCANS = " scans"   --new 0.9
	,INTERFACE_BESTSCAN_THISDISTANCE = "  Within this many meters: "  --new 0.8
	,INTERFACE_RECORDDATE = "Record Scan Date" 
	,INTERFACE_THUMPINGHIDE = "Hide Markers While Thumping"   --new 0.8
	,INTERFACE_HIDEBYDEFAULT = "Hide Markers By Default"   --new 0.9
	,INTERFACE_ANNOUNCEFAILURES = "Show failed scan messages"   --new 0.20
	,INTERFACE_AUTOAGESCANS = "Automatically Remove Old Scans"  --new 0.9
	,INTERFACE_SCANAGE = "Remove Scans Older Than"  --new 0.9
	,INTERFACE_SCANAGE_2 = " hours"  --new 0.9
	,INTERFACE_SCANAGE_3 = " hrs"  --abbreviation for "days" in case it is too long to fit  --new 0.9
	,INTERFACE_HIDEDEFAULTMARKER = "Hide Default Scan Marker"   --new 0.9
	,INTERFACE_CUSTOM_DATE_FORMAT = "Date Format"   --new 0.12
	,INTERFACE_CUSTOM_DATE_FORMAT_TOOLTIP = [["Format options are:
	DD - Two digit day 
	MM - Two digit month
	MON - Month abbreviation, eg JAN, FEB 
	CCYY - Four digit year
	YY - Two digit year"]]  --new 0.12
	,INTERFACE_USE_DEFAULT_SCAN_BILLBOARD = "Use Default Scan Billboard"  --new 0.12
	,INTERFACE_MIN_QUALITY_LIMIT = "Hide qualities lower than:" --new 0.15
	,INTERFACE_MIN_PERCENT_LIMIT = "Hide percentage lower than:" --new 0.17
	,INTERFACE_DETECTRESOURCECHANGES = "Detect resource composition changes"  --new 0.20
	,INTERFACE_DETECTRESOURCECHANGESDISTANCE = "  Within this many meters"  --new 0.20
	,INTERFACE_SHOWTOTALPERCENT = "Show Total Resource Percent"  --new 0.20
	,INTERFACE_WEBICONS_SORT = "  Show icon of highest "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUALITY = "Quality "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUANTITY = "Quantity "  --new -0.20
	,INTERFACE_DISPLAYSTARTUPMESSAGE = "Display Surveyor count startup message"  --new 0.20
	
	,INTERFACE_MARKEROPTIONS_CLASSIC = "Classic Marker"  --new 0.20
	,INTERFACE_MARKEROPTIONS_CHANTICROW = "Chanticrow's Marker"  --new 0.20
	,INTERFACE_MARKERSELECTION_GROUP = "Marker Style Selection"  --new 0.20
	,INTERFACE_MARKERSELECTION = "Pick your marker style:"  --new 0.20

	,INTERFACE_CREDITS = "Credits" 
	,INTERFACE_CREDITS_1 = "Programming: Chanticrow" 
	,INTERFACE_CREDITS_2 = "German Translation: Manni1024" 
	,INTERFACE_CREDITS_3 = "Polish Translation: Drake84pl"   --new 0.9
	,INTERFACE_CREDITS_4 = "French Translation: Nothan"  --new 0.14
	,INTERFACE_CREDITS_5 = "Keeping Surveyor Alive: Rhef, Cryptis Midnight"  --new 0.17
		
	--Map Nodes
	,NODE_SURVEYOR_SHOWMARKERS = "Show/Hide All Surveyor Markers" 
	,NODE_SURVEYOR_KEEPBEST = "Keep Only Best Scans By Quality/Quantity" 
	,NODE_SURVEYOR_DELALLMARKERS = "Delete All Marker and Scan Data" 
	,NODE_SURVEYOR_DELALLMARKERSYES = "Yes, I'm sure I want to clear all scans." 
	,NODE_SURVEYOR_DELALLMARKERSYES2 = "Yes, but only for this shard." 
	,NODE_SURVEYOR_DELALLMARKERSNO = "What am I thinking? Don't clear my data." 
	,NODE_SURVEYOR_FILTERMAIN = "Show Minerals:" 
	,NODE_SURVEYOR_FILTER_SHOWALL = "Show All Minerals"   --new 0.17
	,NODE_SURVEYOR_FILTER_SHOWNONE = "Hide All Minerals"   --new 0.17
	,NODE_SURVEYOR_REMOVE = "Remove This Marker" 
	,NODE_SURVEYOR_CLEANAREA_1 = "Clear " 
	,NODE_SURVEYOR_CLEANAREA_2 = "m Area Of All Markers Except This One"   
	
	--Upgrade
	,UPGRADE_NOTICE_1 = "Surveyor: Upgrading your sites for version " 
	,UPGRADE_NOTICE_2 = "Surveyor: Upgrade complete." 	
	
	--Scan Failure
	,FAILURE_QUALITY = "Scan did not meet minimum quality setting."  --new 0.20
	,FAILURE_PERCENTAGE = "Scan did not meet minimum percentage setting."  --new 0.20
	,FAILURE_BESTSCAN = "Scan resources not better than nearby scans."  --new 0.20
	,FAILURE_RESOURCECHANGE = "Local resource composition has changed. Clearing area."  --new 0.20
	
	--Slash Commands
	,SLASH_DEFAULT = 'Use </surv help> for commands.' 
	,SLASH_HELP = [["Thanks for using Surveyor!
	Surveyor will continue to track your scans even if markers are hidden unless it is disabled. 
	</surv show> to show your markers. 
	</surv hide> to hide your markers.
	</surv disable> to stop recording scans.
	</surv enable> to continue recording scans."]]  
	,SLASH_SHOW = "Surveyor Says: Showing markers..." 
	,SLASH_HIDE = "Surveyor Says: Hiding markers..."
	,SLASH_ENABLE = "Surveyor Says: Enabling scan recording..."
	,SLASH_DISABLE = "Surveyor Says: Scan recording disabled..."
	,SLASH_CLEAR = "Surveyor Says: Cleared all markers in the specified radius."  --new 0.12
	
	--Miscellaneous
	,LOAD_MESSAGE_PART_1 = "Surveyor Says: Found " 
	,LOAD_MESSAGE_PART_2a = " scan recorded for this channel" 
	,LOAD_MESSAGE_PART_2b = " scans recorded for this channel" 
	,LOAD_MESSAGE_PART_2c = " scans recorded for this channel" 
	
	,WAYPOINT_BODYTEXT_1 = "Quality: " 
	,MINERAL_QUALITY_SALVAGE = "Salvage"   --new 0.9
	,MINERAL_QUALITY_COMMON = "Common"   --new 0.9
	,MINERAL_QUALITY_UNCOMMON = "Uncommon"   --new 0.9
	,MINERAL_QUALITY_RARE = "Rare"   --new 0.9
	,MINERAL_QUALITY_EPIC = "Epic"   --new 0.9
	,MINERAL_QUALITY_LEGENDARY = "Legendary"   --new 0.9
	--,MINERAL_FAMILY_POLYMER = "Polymer"   --new 0.12
	--,MINERAL_FAMILY_METAL = "Metal"   --new 0.12
	--,MINERAL_FAMILY_CERAMIC = "Ceramic"   --new 0.12
	--,MINERAL_FAMILY_CARBON = "Carbon"   --new 0.12
	--,MINERAL_FAMILY_BIO = "Bio"   --new 0.12
	,MINERAL_FAMILY_METAL = "Metal"  --new 0.17
	,MINERAL_FAMILY_COMPOSITE = "Composite"  --new 0.17
	,MINERAL_FAMILY_REACTIVE_GAS = "Reactive Gas"  --new 0.17
	,MINERAL_FAMILY_INERT_GAS = "Inert Gas"  --new 0.17
	,MINERAL_FAMILY_BIOMATERIAL = "Biomaterial"  --new 0.17
	,MINERAL_FAMILY_ENZYME = "Enzyme"  --new 0.17
	,MINERAL_FAMILY_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_GROUP_MINERAL = "Mineral"   --new 0.17
	,MINERAL_GROUP_GAS = "Gas"   --new 0.17
	,MINERAL_GROUP_ORGANIC = "Organic"   --new 0.17
	,MINERAL_GROUP_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_CONSTRAINT_POWER = Component.LookupText("STAT_POWER")   --new 0.12
	,MINERAL_CONSTRAINT_MASS = Component.LookupText("STAT_MASS")   --new 0.12
	,MINERAL_CONSTRAINT_CORES = Component.LookupText("STAT_CPU")   --new 0.12

	,DATE_FORMAT = "CCYY-MON-DD"   --2012-JAN-31
	,UNKNOWN_VALUE = "Unknown localization value." 
};

function GetStrings()	
	return LANG_STRINGS;
end