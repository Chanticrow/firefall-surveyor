--Surveyor
--Polish Language Resources
--0.2

local LANG_STRINGS = {	
	INTERFACE_LOCALIZATION = "Język" 
	,INTERFACE_ENGLISH_LANGUAGE = "Angielski" 
	,INTERFACE_GERMAN_LANGUAGE = "Niemiecki" 
	,INTERFACE_POLISH_LANGUAGE = "Polski"   --new 0.9
	,INTERFACE_FRENCH_LANGUAGE = "Francuski"   --new 0.14
	,INTERFACE_LOCALIZATION_TOOLTIP = "Proszę użyć komendy /rui po zmianie Języka." 
	,INTERFACE_DISPLAYOPTIONS = "Opcje Wyświetlania"  --new 0.8
	,INTERFACE_FILTEROPTIONS = "Opcje Filtrowania"  --new 0.8
	,INTERFACE_SURVEYOR_LOCALIZATION = "Język" 
	,INTERFACE_QUALITYCOLORS = "Pokazuj kolory jakości Minerałów" 
	,INTERFACE_SQUADSCANS = "Pokazuj skany członków Drużyny" 
	,INTERFACE_CLEARRADIUS = "Promień usuwania Znaczników" 
	,INTERFACE_CLEARRADIUS_TOOLTIP = "Ustawia promień usuwania Znaczników w opcji na mapie."   --new 0.9
	,INTERFACE_SHOWRADIUS = "Nie pokazuj Znaczników powyżej" 
	,INTERFACE_TITLEFORMAT = "Format Wyświetlania" 
	,INTERFACE_WEBICONS = "Pokazuj kolorowe ikony Minerałów" 
	,INTERFACE_SHOWDEFAULTWAYPOINT = "Pokazuj domyślne Znaczniki Skanów" 
	,INTERFACE_BESTSCAN = "Pokazuj tylko Najlepsze Skany" 
	,INTERFACE_BESTSCAN_THISMANY_SCANS = " Skany"   --new 0.9
	,INTERFACE_BESTSCAN_TOOLTIP = "Każdy następny skan usuwa poprzedni tego samego typu z mniejszą Koncentracją."
	,INTERFACE_BESTSCAN_THISMANY = "Tylko najlepsze: "   --new 0.8
	,INTERFACE_BESTSCAN_THISDISTANCE = "w zasięgu: "  --new 0.8
	,INTERFACE_RECORDDATE = "Zapisuj daty Skanów" 
	,INTERFACE_THUMPINGHIDE = "Ukrywaj Znaczniki podczas Wydobywania"   --new 0.8
	,INTERFACE_HIDEBYDEFAULT = "Nie pokazuj Znaczników Surveyora"   --new 0.9
	,INTERFACE_ANNOUNCEFAILURES = "Show failed scan messages"   --new 0.20
	,INTERFACE_AUTOAGESCANS = "Automatycznie usuwaj stare Skany"  --new 0.9
	,INTERFACE_SCANAGE = "Usuwaj skany starsze niż"  --new 0.9
	,INTERFACE_SCANAGE_2 = "dni"  --new 0.9
	,INTERFACE_SCANAGE_3 = "d"  --abbreviation for "days" in case it is too long to fit  --new 0.9
	,INTERFACE_HIDEDEFAULTMARKER = "Ukrywaj domyślne Znaczniki Skanów"   --new 0.9
	,INTERFACE_CUSTOM_DATE_FORMAT = "Format Daty"   --new 0.12
    ,INTERFACE_CUSTOM_DATE_FORMAT_TOOLTIP = [["Dostępne Opcje:
        DD - Dwucyfrowy Dzień
        MM - Dwucyfrowy Miesiąc
        MON - Skrót Miesiąca np. STY, LUT
        CCYY - Czterocyfrowy Rok
        YY - Dwucyfrowy Rok"]]  --new 0.12
    ,INTERFACE_USE_DEFAULT_SCAN_BILLBOARD = "Użyj domyślnego Raportu Skanu."  --new 0.12
    ,INTERFACE_MIN_QUALITY_LIMIT = "Nie pokazuj Jakości niższych niż:" --new 0.15
    ,INTERFACE_MIN_PERCENT_LIMIT = "Nie pokazuj Procentów niższych niż:" --new 0.17
	,INTERFACE_DETECT_RESOURCE_CHANGES = "Detect resource composition changes"  --new 0.20
	,INTERFACE_SHOWTOTALPERCENT = "Show Total Resource Percent"  --new 0.20
	,INTERFACE_WEBICONS_SORT = "  Show icon of highest "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUALITY = "Quality "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUANTITY = "Quantity "  --new -0.20
	,INTERFACE_DISPLAYSTARTUPMESSAGE = "Display Surveyor count startup message"  --new 0.20
	
	,INTERFACE_CREDITS = "Informacje" 
	,INTERFACE_CREDITS_1 = "Programowanie: Chanticrow" 
	,INTERFACE_CREDITS_2 = "Niemieckie Tłumaczenie: Manni1024" 
	,INTERFACE_CREDITS_3 = "Polskie Tłumaczenie: Drake84pl"   --new 0.9
	,INTERFACE_CREDITS_4 = "Francuskie Tłumaczenie: Nothan"  --new 0.14
	,INTERFACE_CREDITS_5 = "Keeping Surveyor Alive: Rhef, Cryptis Midnight"  --new 0.17
		
	--Map Nodes
	,NODE_SURVEYOR_SHOWMARKERS = "Pokaż/Ukryj wszystkie Znaczniki Surveyora." 
	,NODE_SURVEYOR_KEEPBEST = "Pokaż tylko Najlepsze skany według ich Jakości i Koncentracji." 
	,NODE_SURVEYOR_DELALLMARKERS = "Usuń wszystkie Znaczniki i Dane Skanów." 
	,NODE_SURVEYOR_DELALLMARKERSYES = "TAK jestem pewny, Usuń wszystkie Skany." 
	,NODE_SURVEYOR_DELALLMARKERSYES2 = "TAK, ale tylko dla tej Instancji." 
	,NODE_SURVEYOR_DELALLMARKERSNO = "Co ja wyprawiam ? NIE kasuj moich danych." 
	,NODE_SURVEYOR_FILTERMAIN = "Pokaż/Ukryj dany Minerał:" 
	,NODE_SURVEYOR_FILTER_SHOWALL = "Pokaż wszystkie Surowce"  --new 0.17
	,NODE_SURVEYOR_FILTER_SHOWNONE = "Ukryj wszystkie Surowce"  --new 0.17
	,NODE_SURVEYOR_REMOVE = "Usuń ten Znacznik." 
	,NODE_SURVEYOR_CLEANAREA_1 = "Usuń wszystkie Znaczniki w promieniu " 
	,NODE_SURVEYOR_CLEANAREA_2 = "m, oprócz tego."   
	
	--Upgrade
	,UPGRADE_NOTICE_1 = "Surveyor: Atualizacja Terenu dla wersji " 
	,UPGRADE_NOTICE_2 = "Surveyor: Aktualizacja zakończona." 
	
	--Scan Failure
	,FAILURE_QUALITY = "Scan did not meet minimum quality setting."  --new 0.20
	,FAILURE_PERCENTAGE = "Scan did not meet minimum percentage setting."  --new 0.20
	,FAILURE_BESTSCAN = "Scan resources not better than nearby scans."  --new 0.20	
	
	--Slash Commands
	,SLASH_DEFAULT = 'Użyj </surv help> aby wyświetlić listę komend.' 
	,SLASH_HELP = [["Dziękuję za wybranie Surveyora!
	Surveyor będzie zapisywał twoje skany nawet gdy znaczniki są ukryte, no chyba że go wyłączysz. 
	</surv show> aby pokazać Znaczniki. 
	</surv hide> aby ukryć Znaczniki.
	</surv disable> aby wyłączyć zapisywanie Skanów.
	</surv enable> aby włączyć zapisywanie Skanów.
	</surv list> aby zobaczyć listę ostatnich Minerałów."]]  --updated 0.12
	,SLASH_SHOW = "Surveyor: Wyświetlono Znaczniki Skanów" 
	,SLASH_HIDE = "Surveyor: Ukryto Znaczniki Skanów"
	,SLASH_ENABLE = "Surveyor: Zapisywanie skanów Włączone"
	,SLASH_DISABLE = "Surveyor: Zapisywanie skanów Wyłączone"
	,SLASH_CLEAR = "Surveyor: Usunięto wszystkie znaczniki w określonym promieniu."  --new 0.12
	
	--Miscellaneous
	,LOAD_MESSAGE_PART_1 = "Surveyor: Znaleziono " 
	,LOAD_MESSAGE_PART_2a = " zapisany Skan w tej Instancji"   --1 item
	,LOAD_MESSAGE_PART_2b = " zapisane Skany w tej Instancji"  --2, 3, or 4 items
	,LOAD_MESSAGE_PART_2c = " zapisanych Skanów w tej Instancji"   --5 or more items
	
	,WAYPOINT_BODYTEXT_1 = "Jakość: " 
	,MINERAL_QUALITY_SALVAGE = "z Odzysku"   --new 0.9
	,MINERAL_QUALITY_COMMON = "Popularny"   --new 0.9
	,MINERAL_QUALITY_UNCOMMON = "Nie Spotykany"   --new 0.9
	,MINERAL_QUALITY_RARE = "Rzadki"   --new 0.9
	,MINERAL_QUALITY_EPIC = "Epicki"   --new 0.9
	,MINERAL_QUALITY_LEGENDARY = "Legendarny"   --new 0.9
	--,MINERAL_FAMILY_POLYMER = "Polymer"   --new 0.12
	--,MINERAL_FAMILY_METAL = "Metal"   --new 0.12
	--,MINERAL_FAMILY_CERAMIC = "Ceramic"   --new 0.12
	--,MINERAL_FAMILY_CARBON = "Carbon"   --new 0.12
	--,MINERAL_FAMILY_BIO = "Bio"   --new 0.12
	,MINERAL_FAMILY_METAL = "Metal"  --new 0.17
	,MINERAL_FAMILY_COMPOSITE = "Composite"  --new 0.17
	,MINERAL_FAMILY_REACTIVE_GAS = "Reactive Gas"  --new 0.17
	,MINERAL_FAMILY_INERT_GAS = "Inert Gas"  --new 0.17
	,MINERAL_FAMILY_BIOMATERIAL = "Biomaterial"  --new 0.17
	,MINERAL_FAMILY_ENZYME = "Enzyme"  --new 0.17
	,MINERAL_FAMILY_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_GROUP_MINERAL = "Mineral"   --new 0.17
	,MINERAL_GROUP_GAS = "Gas"   --new 0.17
	,MINERAL_GROUP_ORGANIC = "Organic"   --new 0.17	
	,MINERAL_GROUP_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_CONSTRAINT_POWER = Component.LookupText("STAT_POWER")   --new 0.12
	,MINERAL_CONSTRAINT_MASS = Component.LookupText("STAT_MASS")   --new 0.12
	,MINERAL_CONSTRAINT_CORES = Component.LookupText("STAT_CPU")   --new 0.12

	,DATE_FORMAT = "DD.MM.YYYY"   --Jan-01-2012
	,UNKNOWN_VALUE = "Nieznana komenda."
	};
	
function GetStrings()	
	return LANG_STRINGS;
end




        
        
        