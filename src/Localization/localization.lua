-- Localization
-- Author: Chanticrow
-- A simple localization library
-- Version 0.2

-- 0.1
--  Base functionality
--  English and German languages
	
-- 0.2
--  Moved language strings to their own files.
--  Made this library more generic for easier use and maintenance.


--[[
HOW TO USE

Add this as a require to the addon.  For example:

	require "./Localization/localization";
	
To set the language call Set_Localization_Values() with the language name 
and the name for your setting storage.  This can be anything, but please use something
that identifies your addon.  For example:

	Set_Localization_Values("English", "Addon_YourAddonName_Localization");

Using nil in the language name will set the language to whatever the default language is
as determined by the "local_lang" variable.

To look up a value call Get_Localization_Value() with the name of the string for lookup.
For example:
	
	Get_Localization_Value("NODES_SET_LANGUAGE");
	
To add new languages create a new file in the Localization folder for the desired language.
Create the strings in an array called LANG_STRINGS.  Return LANG_STRINGS. 
Modify the specified area in Set_Localization_Values() to require your language file.

]]

local local_lang = "English";					
local LOCAL_STRINGS = {};

----------------------------------------------------------
function Set_Localization_Values(language_val, localization_Settings)
	
	log("   Incoming localization value: "..tostring(language_val))

	Load_Localization_Setting(localization_Settings);

	if language_val == nil then
		language_val = local_lang;
	end

	local_lang = language_val;
	
	Save_Localization_Setting(localization_Settings);

	log("   Setting localization values: "..tostring(language_val));	

	--------------------------------
	-- Add new languages here
	--------------------------------
	if tostring(local_lang) == "English" then
		require("./Localization/EN");
	elseif tostring(local_lang) == "French" then
		require("./Localization/FR");
	elseif tostring(local_lang) == "German" then
		require("./Localization/DE");
	elseif tostring(local_lang) == "Polish" then
		require("./Localization/PL");
	end
	--------------------------------
	--
	--------------------------------
	
	LOCAL_STRINGS = GetStrings();
	
	if LOCAL_STRINGS.UNKNOWN_VALUE then
		unknown_value = LOCAL_STRINGS.UNKNOWN_VALUE;
	else
		unknown_value = "Strings not found.";
	end
end


----------------------------------------------------------
function Get_Localization_Value(value)

	--log("What we're looking for: "..tostring(value))
	--log("Complete Values: "..tostring(LOCAL_STRINGS))
	if LOCAL_STRINGS[tostring(value)] ~= nil then
		return LOCAL_STRINGS[tostring(value)];
	else
		return unknown_value;
	end
end

----------------------------------------------------------
function Load_Localization_Setting(localization_Settings)
	if Component.GetSetting(localization_Settings) ~= nil then
		local_lang = Component.GetSetting(localization_Settings);

		log("   Loaded language setting: "..tostring(local_lang))
	else
		log("   No localization found.  Defaulting to English.")
		local_lang = "English"
		Save_Localization_Setting(localization_Settings);
	end
end

----------------------------------------------------------
function Save_Localization_Setting(localization_Settings)
	Component.SaveSetting(localization_Settings, local_lang);
	log("   Saved localization as "..tostring(local_lang))
end


