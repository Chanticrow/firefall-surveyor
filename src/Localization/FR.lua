--Surveyor
--French Language Resources
--0.1

local LANG_STRINGS = {	
	INTERFACE_LOCALIZATION = "Selectionnez une langue" 
	,INTERFACE_ENGLISH_LANGUAGE = "Anglais" 
	,INTERFACE_GERMAN_LANGUAGE = "Allemand" 
	,INTERFACE_POLISH_LANGUAGE = "Polonais"   --new 0.9
	,INTERFACE_FRENCH_LANGUAGE = "Francais"   --new 0.14
	,INTERFACE_LOCALIZATION_TOOLTIP = "SVP faites /rui apres un changement de langue." 
	,INTERFACE_DISPLAYOPTIONS = "Options d'affichage"  --new 0.8
	,INTERFACE_FILTEROPTIONS = "Options de filtrage"  --new 0.8
	,INTERFACE_SURVEYOR_LOCALIZATION = "Langage" 
	,INTERFACE_QUALITYCOLORS = "Utiliser la couleur pour la qualite des mineraux" 
	,INTERFACE_SQUADSCANS = "Enregistrer les scans des membres du Squad" 
	,INTERFACE_CLEARRADIUS = "Taille de zone d'effacement" 
	,INTERFACE_CLEARRADIUS_TOOLTIP = "Sur la map, si vous le demandez cela efface les marqueurs qui sont dans une zone choisie."   --new 0.9
	,INTERFACE_SHOWRADIUS = "Afficher les marqueurs si inf. a" 
	,INTERFACE_TITLEFORMAT = "Format d'affichage du marqueur" 
	,INTERFACE_WEBICONS = "Utiliser les icons des materiaux sur la carte" 
	,INTERFACE_SHOWDEFAULTWAYPOINT = "Affichage par defaut des points de repere" 
	,INTERFACE_BESTSCAN = "Ne garder que le meilleur des scans" 
	,INTERFACE_BESTSCAN_TOOLTIP = "cela efface automatiquement les scans qui sont moins bons en type et en pourcentage."
	,INTERFACE_BESTSCAN_THISMANY = "Nombre de scans a garder: "   --new 0.8
	,INTERFACE_BESTSCAN_THISMANY_SCANS = " scans"   --new 0.9
	,INTERFACE_BESTSCAN_THISDISTANCE = "dans un rayon maxi de: "  --new 0.8
	,INTERFACE_RECORDDATE = "Enregistrer les dates de Scan" 
	,INTERFACE_THUMPINGHIDE = "Cacher les marqueurs pendant le Thump"   --new 0.8
	,INTERFACE_HIDEBYDEFAULT = "Cacher les marqueurs par defaut"   --new 0.9
	,INTERFACE_ANNOUNCEFAILURES = "Show failed scan messages"   --new 0.20
	,INTERFACE_AUTOAGESCANS = "Enlever automatiquement les vieux scans"  --new 0.9
	,INTERFACE_SCANAGE = "datant de plus de"  --new 0.9
	,INTERFACE_SCANAGE_2 = "jours"  --new 0.9
	,INTERFACE_SCANAGE_3 = "Jr"  --abbreviation for "days" in case it is too long to fit  --new 0.9
	,INTERFACE_HIDEDEFAULTMARKER = "Cacher les marqueurs d'origine"   --new 0.9
	,INTERFACE_CUSTOM_DATE_FORMAT = "Format des dates"   --new 0.12
	,INTERFACE_CUSTOM_DATE_FORMAT_TOOLTIP = [["Options des formats sont:
	DD - 2chiffres pour le jour
	MM - 2chiffres pour le mois
	MON - le mois (anglais: FEB,APR)
	CCYY - 4chiffres pour l'annee
YY pour 2chiffres"]]  --new 0.12
	,INTERFACE_USE_DEFAULT_SCAN_BILLBOARD = "Utiliser par defaut le scan Billboard."  --new 0.12
	,INTERFACE_MIN_QUALITY_LIMIT = "Cacher les quantites inferieur a:" --new 0.15
	,INTERFACE_MIN_PERCENT_LIMIT = "Cacher si inferieur � %:" --new 0.17
	,INTERFACE_DETECT_RESOURCE_CHANGES = "Detect resource composition changes"  --new 0.20
	,INTERFACE_SHOWTOTALPERCENT = "Show Total Resource Percent"  --new 0.20
	,INTERFACE_WEBICONS_SORT = "  Show icon of highest "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUALITY = "Quality "  --new -0.20
	,INTERFACE_WEBICONS_SORTQUANTITY = "Quantity "  --new -0.20
	,INTERFACE_DISPLAYSTARTUPMESSAGE = "Display Surveyor count startup message"  --new 0.20
	
	,INTERFACE_CREDITS = "Credits" 
	,INTERFACE_CREDITS_1 = "Programming: Chanticrow" 
	,INTERFACE_CREDITS_2 = "German Translation: Manni1024" 
	,INTERFACE_CREDITS_3 = "Polish Translation: Drake84pl"   --new 0.9
	,INTERFACE_CREDITS_4 = "French Translation: Nothan"  --new 0.14
	,INTERFACE_CREDITS_5 = "Keeping Surveyor Alive: Rhef, Cryptis Midnight"  --new 0.17

	--Map Nodes
	,NODE_SURVEYOR_SHOWMARKERS = "Afficher/Cacher tous les marqueurs de Surveyor" 
	,NODE_SURVEYOR_KEEPBEST = "Ne garder que les meilleurs scans par qualite/quantite" 
	,NODE_SURVEYOR_DELALLMARKERS = "Effacer tous les marqueurs ainsi que les scans" 
	,NODE_SURVEYOR_DELALLMARKERSYES = "Oui, je confirme l'effacement de tous les scans sur tous les serveurs." 
	,NODE_SURVEYOR_DELALLMARKERSYES2 = "Oui, mais seulement sur ce serveur." 
	,NODE_SURVEYOR_DELALLMARKERSNO = "Desole je revais, n'effacer rien." 
	,NODE_SURVEYOR_FILTERMAIN = "Afficher les mineraux:" 
	,NODE_SURVEYOR_FILTER_SHOWALL = "Voir tous les mineraux"  --new 0.17
	,NODE_SURVEYOR_FILTER_SHOWNONE = "Cacher tous les mineraux"  --new 0.17
	,NODE_SURVEYOR_REMOVE = "Retirer ce marqueur" 
	,NODE_SURVEYOR_CLEANAREA_1 = "Effacer sur " 
	,NODE_SURVEYOR_CLEANAREA_2 = "m tous les marqueurs sauf celui-ci" 
	
	--Upgrade
	,UPGRADE_NOTICE_1 = "Surveyor: mise a jour en cours " 
	,UPGRADE_NOTICE_2 = "Surveyor: mise a jour complete." 
	
	--Scan Failure
	,FAILURE_QUALITY = "Scan did not meet minimum quality setting."  --new 0.20
	,FAILURE_PERCENTAGE = "Scan did not meet minimum percentage setting."  --new 0.20
	,FAILURE_BESTSCAN = "Scan resources not better than nearby scans."  --new 0.20
	
	--Slash Commands
	,SLASH_DEFAULT = 'Ecrire </surv help> sans les <> pour afficher les commandes.' 
	,SLASH_HELP = [["Merci d'utiliser Surveyor!
	Surveyor continue de prendre vos scans meme si ils sont pas affiches, a moins qu'il ne soit desactive.
	</surv show> Pour afficher les marqueurs
	</surv hide> Pour cacher les marqueurs.
	</surv disable> Arreter les enregistrements de scans.
	</surv enable> Mettre les enregistrements de scans en marche.
	</surv list> Pour voir la liste des mineraux rammasses."]]  --updated 0.12
	,SLASH_SHOW = "Surveyor dit: Marqueurs affiches..." 
	,SLASH_HIDE = "Surveyor dit: Marqueurs caches..."
	,SLASH_ENABLE = "Surveyor dit: enregistrement des scans en marche..."
	,SLASH_DISABLE = "Surveyor dit: arret des enregistrements de scans..."
	,SLASH_CLEAR = "Surveyor dit: marqueurs effaces dans ce perimetre..."  --new 0.12
	
	--Miscellaneous
	,LOAD_MESSAGE_PART_1 = "Surveyor dit: Il y a " 
	,LOAD_MESSAGE_PART_2a = " scan enregistre pour ce serveur" 
	,LOAD_MESSAGE_PART_2b = " scans enregistres pour ce serveur" 
	,LOAD_MESSAGE_PART_2c = " scans enregistres pour ce serveur" 
	
	,WAYPOINT_BODYTEXT_1 = "Qualite: " 
	,MINERAL_QUALITY_SALVAGE = "Salvage"   --new 0.9
	,MINERAL_QUALITY_COMMON = "Common"   --new 0.9
	,MINERAL_QUALITY_UNCOMMON = "Uncommon"   --new 0.9
	,MINERAL_QUALITY_RARE = "Rare"   --new 0.9
	,MINERAL_QUALITY_EPIC = "Epic"   --new 0.9
	,MINERAL_QUALITY_LEGENDARY = "Legendary"   --new 0.9
	--,MINERAL_FAMILY_POLYMER = "Polymer"   --new 0.12
	--,MINERAL_FAMILY_METAL = "Metal"   --new 0.12
	--,MINERAL_FAMILY_CERAMIC = "Ceramic"   --new 0.12
	--,MINERAL_FAMILY_CARBON = "Carbon"   --new 0.12
	--,MINERAL_FAMILY_BIO = "Bio"   --new 0.12
	,MINERAL_FAMILY_METAL = "Metal" --new 0.17
	,MINERAL_FAMILY_COMPOSITE = "Composite" --new 0.17
	,MINERAL_FAMILY_REACTIVE_GAS = "Gaz Reactif" --new 0.17
	,MINERAL_FAMILY_INERT_GAS = "Gaz inerte" --new 0.17
	,MINERAL_FAMILY_BIOMATERIAL = "Biomaterial" --new 0.17
	,MINERAL_FAMILY_ENZYME = "Enzyme" --new 0.17
	,MINERAL_FAMILY_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_GROUP_MINERAL = "Mineraux"  --new 0.17
	,MINERAL_GROUP_GAS = "Gaz"  --new 0.17
	,MINERAL_GROUP_ORGANIC = "Organique"  --new 0.17
	,MINERAL_GROUP_CRYSTITE = "Crystite"   --new 0.19
	,MINERAL_CONSTRAINT_POWER = Component.LookupText("STAT_POWER")   --new 0.12
	,MINERAL_CONSTRAINT_MASS = Component.LookupText("STAT_MASS")   --new 0.12
	,MINERAL_CONSTRAINT_CORES = Component.LookupText("STAT_CPU")   --new 0.12

	,DATE_FORMAT = "CCYY-MON-DD"   --2012-JAN-31
	,UNKNOWN_VALUE = "Donnee de langage inconnue."
	};
	
function GetStrings()	
	return LANG_STRINGS;
end