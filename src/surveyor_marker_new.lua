if SURV_SUBMARKER then
	-- only include once
	return nil
end

SURV_SUBMARKER = {};

require "lib/lib_TextFormat";

local INTERFACE_VARS = {};
local SURV_MARKER_API = {};
local MARKER = {};
local HEIGHT_OFFSET = 1.5;

----------------------------------------------------------
SURV_SUBMARKER.SetInterfaceValues = function(inINTERFACE_VARS)
	INTERFACE_VARS = inINTERFACE_VARS;
end

----------------------------------------------------------
SURV_SUBMARKER.BuildMapMarker = function (thisMarker)
	Debug.Table("Creating Chanticrow map marker", thisMarker);
	
	local MARKER = {};
	MARKER = {
		 markerId = thisMarker.markerId
		,FRAME = Component.CreateFrame("TrackingFrame")
		,ENTRIES = {}
		,pos = {x=thisMarker.x, y=thisMarker.y, z=thisMarker.z + HEIGHT_OFFSET}
	} 
	
	MARKER.FRAME:Show(false);
	MARKER.ANCHOR = MARKER.FRAME:GetAnchor();
	MARKER.ANCHOR:BindToWorld(false);
	MARKER.ANCHOR:SetParam("translation", MARKER.pos);
	MARKER.ANCHOR:LookAt("screen", 0.1);
	--MARKER.ANCHOR:SetParam("rotation", {axis={x=0,y=0,z=1}, angle=20});
	
	MARKER.WIDGET = Component.CreateWidget("Surv_MapMarker_New", MARKER.FRAME);
	MARKER.ENTRY_GROUP = MARKER.WIDGET:GetChild("Entries");
	MARKER.CONTEXT = MARKER.WIDGET:GetChild("Context");
	
	local heightCount = -100;
	local siftedHeight = 200;
	local adjustment = 0;
	
	for i, details in pairs(thisMarker.resources) do
		--name, resource_color, display, web_icon, percent, resource_quality
		if details.name == "Total" then
			--Ignore the total entry			
		else
			local BASE_ENTRY = Component.CreateWidget("Map_Entry", MARKER.ENTRY_GROUP);
			
			local ENTRY = {};
			ENTRY = {
				 BASE = BASE_ENTRY
				,BAR = BASE_ENTRY:GetChild("bar")
			} 
			
			adjustment = (details.percent * 100) * 2;
			if adjustment < 15 then
				adjustment = 15;
			end
			heightCount = heightCount + adjustment;
			siftedHeight = siftedHeight - adjustment;
			Debug.Log("Setting adjustment "..adjustment.." and heightCount "..heightCount);
			ENTRY.BAR:SetDims("bottom:"..heightCount.."%; height:"..adjustment.."%;");
			ENTRY.BAR:SetParam("tint", details.qualityColor);
			
			MARKER.ENTRIES[i] = ENTRY;
		end
	end
	
	MARKER.SIFTED = MARKER.WIDGET:GetChild("SiftedEarth_Entry");
	MARKER.SIFTED:SetDims("bottom: 100%; height:"..(siftedHeight).."%;")
	
	
		
	MARKER.FRAME:SetScene("map");
	--MARKER.FRAME:AddToScene("map");
	MARKER.FRAME:SetParam("cullalpha", 1);
	MARKER.FRAME:SetDims("left:-20%; height:50%");
	--MARKER.FRAME:SetScaleRamp(1, 100, 0.1, 4.5);
	--MARKER.FRAME:SetScaleRamp(0, 100, .15, 4);
	--MARKER.FRAME:SetScaleRamp(8, 1000, 0.3, 20);  --like this one
	--MARKER.FRAME:SetScaleRamp(10, 500, 0.6, 10);
	--MARKER.FRAME:SetScaleRamp(beyond this distance start scaling, ??, nearest scaling size, ?? )
	--MARKER.FRAME:SetScaleRamp(INTERFACE_VARS.ScaleRampA, INTERFACE_VARS.ScaleRampB, INTERFACE_VARS.ScaleRampC, INTERFACE_VARS.ScaleRampD);
	MARKER.FRAME:Show(false);
	MARKER.CONTEXT:Show(false);
		
	return MARKER;
	
end

----------------------------------------------------------
SURV_SUBMARKER.BuildMarker = function (thisMarker)
	Debug.Table("Creating Chanticrow marker", thisMarker);
	local markerObj = {};
	
	for k,method in pairs(SURV_MARKER_API) do
		markerObj[k] = method;
	end
	
	local MARKER = {};
	MARKER = {
		 markerId = thisMarker.markerId
		,FRAME = Component.CreateFrame("TrackingFrame")
		,ENTRIES = {}
		,pos = {x=thisMarker.x, y=thisMarker.y, z=thisMarker.z + HEIGHT_OFFSET}
	} 
	
	MARKER.FRAME:Show(false);
	MARKER.ANCHOR = MARKER.FRAME:GetAnchor();
	MARKER.ANCHOR:BindToWorld(false);
	MARKER.ANCHOR:SetParam("translation", MARKER.pos);
	MARKER.ANCHOR:LookAt("camera", 0.1);
	MARKER.ANCHOR:SetParam("rotation", {axis={x=0,y=0,z=1}, angle=20});
	
	MARKER.WIDGET = Component.CreateWidget("Surv_Marker_New", MARKER.FRAME);
	MARKER.ENTRY_GROUP = MARKER.WIDGET:GetChild("Entries");
	
	local heightCount = -100;
	local siftedHeight = 200;
	local adjustment = 0;
	
	for i, details in pairs(thisMarker.resources) do
		--name, resource_color, display, web_icon, percent, resource_quality
		
		if details.name == "Total" then
			local TOTAL_ENTRY = MARKER.WIDGET:GetChild("Total_Entry", MARKER.FRAME);
			TOTAL_ENTRY:SetText(details.display);
			
		else
			local BASE_ENTRY = Component.CreateWidget("Resource_Entry", MARKER.ENTRY_GROUP);
			
			local ENTRY = {};
			ENTRY = {
				 BASE = BASE_ENTRY
				,BAR = BASE_ENTRY:GetChild("bar")
				,NAME = BASE_ENTRY:GetChild("name")
				,BACKING = BASE_ENTRY:GetChild("backing")
			} 
			
			adjustment = (details.percent * 100) * 2;
			if adjustment < 15 then
				adjustment = 15;
			end
			heightCount = heightCount + adjustment;
			siftedHeight = siftedHeight - adjustment;
			Debug.Log("Setting adjustment "..adjustment.." and heightCount "..heightCount);
			ENTRY.BAR:SetDims("bottom:"..heightCount.."%; height:"..adjustment.."%;");
			ENTRY.BAR:SetParam("tint", details.qualityColor);
			ENTRY.NAME:SetText(details.display);
			ENTRY.NAME:SetDims("bottom:"..heightCount.."%; height:"..adjustment.."%;")
			ENTRY.BACKING:SetDims("height: 15%; bottom:"..(heightCount - adjustment + 15).."%;");
			ENTRY.BACKING:SetParam("tint", details.qualityColor);
			
			MARKER.ENTRIES[i] = ENTRY;
		end		
	end
	
	MARKER.SIFTED = MARKER.WIDGET:GetChild("SiftedEarth_Entry");
	MARKER.SIFTED:SetDims("bottom: 100%; height:"..(siftedHeight).."%;")
	
	
		
	MARKER.FRAME:SetScene("world");
	--MARKER.FRAME:AddToScene("map");
	MARKER.FRAME:SetParam("cullalpha", 0.5);
	--MARKER.FRAME:SetScaleRamp(1, 100, 0.1, 4.5);
	--MARKER.FRAME:SetScaleRamp(0, 100, .15, 4);
	--MARKER.FRAME:SetScaleRamp(8, 1000, 0.3, 20);  --like this one
	MARKER.FRAME:SetScaleRamp(10, 500, 0.6, 10);
	--MARKER.FRAME:SetScaleRamp(beyond this distance start scaling, ??, nearest scaling size, ?? )
	--MARKER.FRAME:SetScaleRamp(INTERFACE_VARS.ScaleRampA, INTERFACE_VARS.ScaleRampB, INTERFACE_VARS.ScaleRampC, INTERFACE_VARS.ScaleRampD);
	MARKER.FRAME:Show(false);
		
	local MAPMARKER = {};
	MAPMARKER = SURV_SUBMARKER.BuildMapMarker(thisMarker);
	MARKER.MAPMARKER = MAPMARKER;	
	
	return MARKER;
	
end


----------------------------------------------------------
SURV_SUBMARKER.DestroyMarker = function(MARKER)
	Component.RemoveFrame(MARKER.MAPMARKER.FRAME);
	Component.RemoveFrame(MARKER.FRAME);
end

----------------------------------------------------------
SURV_SUBMARKER.ShowMarker = function(MARKER, worldmapFlag, radarFlag, hudFlag)
	MARKER.FRAME:Show(hudFlag);
	MARKER.MAPMARKER.FRAME:Show(worldmapFlag);
end