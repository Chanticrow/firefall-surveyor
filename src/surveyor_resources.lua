if SURV_RES then
	-- only include once
	return nil
end

SURV_RES = {};

--[[

Functions:

SetInterfaceValues = function(inInstanceId, inBestScanQty, inBestScanDistance)
ResourceList_Load = function(instanceId)
ResourceList_Save = function()
StoreResource = function(thisResource)

RemoveResourceFromScan = function(scanId, itemTypeId)
MarkResourceBestFlag = function(scanId, itemTypeId, bestFlag)
IsScanStillABestScan = function(resources)

ProcessScan = function(args)
GetListOfResourceType = function(itemTypeId, resource_quality, sortFlag)
GetListOfResourcesByScanId = function(scanId, sortFlag)
GetListOfDistinctScans = function()

]]


local resourceList = {};
local storedResources = {};

local INTERFACE_VARS = {};
local USEFUL_VALUES = {};
local const_SiftedEarth = "30404";  --constant - identity of Sifted Earth resource
local nearestScan = nil;

----------------------------------------------------------
local function RecalucateCoordinates(coords)
	local tempCoords = {};

	tempCoords = Game.ChunkCoordToWorld(coords.chunkX,coords.chunkY,coords.storeX,coords.storeY,coords.storeZ)
	coords.x = tempCoords.x;
	coords.y = tempCoords.y;
	coords.z = tempCoords.z;

	return coords;	
end


SURV_RES.SetInterfaceValues = function(inUSEFUL_VALUES, inINTERFACE_VARS)
	USEFUL_VALUES = inUSEFUL_VALUES;
	INTERFACE_VARS = inINTERFACE_VARS;
	--Debug.Table("Setting interface variables ", INTERFACE_VARS);
	--Debug.Table("Setting useful values ", USEFUL_VALUES);
end

----------------------------------------------------------
-- STORAGE FUNCTIONS
-- -------------------------------------------------------


----------------------------------------------------------
SURV_RES.ResourceList_Load = function(instanceId)
	Debug.Log("Attempting to load scans for instance ", instanceId)
	storedResources = {};
	resourceList = {};
	
	if Component.GetSetting("Addon_Surveyor_Resources") ~= nil then
		storedResources = Component.GetSetting("Addon_Surveyor_Resources");
		resourceList = storedResources[instanceId] or {};
		Debug.Log("Found "..#resourceList.." entries.")
	end
	
	--Recalculate all x,y,x coords from the chunk system.
	for i, resource in pairs(resourceList) do
		resource.coords = RecalucateCoordinates(resource.coords);
	end
end

----------------------------------------------------------
SURV_RES.ResourceList_Save = function()
	storedResources[USEFUL_VALUES.l_instanceId] = {};
	storedResources[USEFUL_VALUES.l_instanceId] = resourceList;
	Component.SaveSetting("Addon_Surveyor_Resources", storedResources);
end

----------------------------------------------------------
SURV_RES.StoreResource = function(thisResource)
	--Debug.Log("       Saving resource ", thisResource.name);
	table.insert(resourceList, thisResource);
	SURV_RES.ResourceList_Save();
end

----------------------------------------------------------
SURV_RES.RemoveResourceFromScan = function(scanId, itemTypeId)
	for i, resource in pairs(resourceList) do
		if resource.scanId == scanId and
		   resource.itemTypeId == itemTypeId then
			table.remove(resourceList,i);
			SURV_RES.ResourceList_Save();
			break;
		end
	end
end

----------------------------------------------------------
SURV_RES.DeleteScan = function(scanId)
	--Debug.Log("      Deleting scan", scanId);
	local scroller = {};
	for j, x in pairs(resourceList) do scroller[j] = x end
	
	for i, resource in pairs(scroller) do
		if resource.scanId == scanId then
			--Debug.Table("Planning to delete resource ", resource);
			--Debug.Table("Actually deleting resource ", resourceList[i]);
			--table.remove(resourceList,i);
			resourceList[i] = nil;
		end
	end
	
	SURV_RES.ResourceList_Save();
end

----------------------------------------------------------
SURV_RES.DeleteAllScans = function(instanceId)
	--Debug.Log("      Deleting all scans", instanceId);
	
	if instanceId ~= nil then
		resourceList = {};
	else
		resourceList = {};
		storedResources = {};
	end
	
	SURV_RES.ResourceList_Save();
end

----------------------------------------------------------
-- LOOKUP FUNCTIONS
-- -------------------------------------------------------

----------------------------------------------------------
local function IsSameOwner(ownerId)
	if tostring(ownerId) == tostring(Player.GetTargetId()) then
		return true;
	else
		return false;
	end
end

----------------------------------------------------------
local function IsThisSiftedEarth(itemTypeId)
	--Special use function.  If it is sifted earth we do not want to 
	--keep the resource so we return false.
	if tostring(itemTypeId) == const_SiftedEarth then
		--Debug.Log("      Rejecting Sifted Earth")
		return true;
	else
		return false;
	end
end

----------------------------------------------------------
SURV_RES.MarkResourceBestFlag = function(scanId, itemTypeId, bestFlag)
	for i, resource in pairs(resourceList) do
		if resource.scanId == scanId and
		   resource.itemTypeId == itemTypeId then
			resource.bestFlag = bestFlag;
			SURV_RES.ResourceList_Save();
			break;
		end
	end
end

----------------------------------------------------------
SURV_RES.GetListOfResourceType = function(itemTypeId, resource_quality, sortFlag)
	--Debug.Log("      Get List of Resources by Type "..tostring(itemTypeId).." and quality "..tostring(resource_quality)..".");
	
	local resultList = {};
	
	if itemTypeId == nil then
		return resultList;
	end
	
	if resourceList == nil or resourceList == {} then
		--Debug.Log("      No resources recorded so far.");
		return resultList;
	end 
	
	if resource_quality ~= nil and resource_quality > 0 then
		for i, resource in pairs(resourceList) do
			if tostring(itemTypeId) == tostring(resource.itemTypeId) then
		   		table.insert(resultList, resource);
		   	end
		end
	else
		for i, resource in pairs(resourceList) do
			if tostring(itemTypeId) == tostring(resource.itemTypeId) and
		   	   tostring(resource_quality) == tostring(resource.resource_quality) then
		   		table.insert(resultList, resource);
		   	end
		end
	end
	
	if sortFlag == true then
		table.sort(resultList, function(A, B)  
			if A.resource_quality == B.resource_quality then
				return A.percent > B.percent;
			else
				return A.resource_quality > B.resource_quality;
			end
		end);
	end
	--Debug.Table("      Found List of Resources by Type", resultList);
	
	return resultList;
end

----------------------------------------------------------
SURV_RES.GetListOfResourcesByScanId = function(scanId, sortFlag)
	--Debug.Log("   Get List of Resources by Scan Id", scanId);
	
	local resultList = {};
	
	if scanId == nil then
		return resultList;
	end
	
	for i, resource in pairs(resourceList) do
		if scanId == resource.scanId then
	   		table.insert(resultList, resource);
	   	end
	end
	
	if sortFlag == true then
		table.sort(resultList, function(A, B)  
				return A.percent > B.percent;
		end);
	end
	--Debug.Table("   Found List of Resources by Scan Id", resultList);
	
	return resultList;
end

----------------------------------------------------------
SURV_RES.GetListOfDistinctScans = function()
	--Debug.Log("      Get List of Distinct Scans ");
	
	local scanList = {};
	
	if resourceList == nil or resourceList == {} then
		--Debug.Log("      No resources recorded so far.");
		return scanList;
	end	
	
	for i, resource in pairs(resourceList) do
		if scanList[resource.scanId] == nil then
			scanList[resource.scanId] = 1;
		else
			scanList[resource.scanId] = scanList[resource.scanId] + 1;
	   	end
	end	
	
	return scanList;
end

----------------------------------------------------------
local function CompareNearbyScans(resources)
	--Compare this scan to the closest other scan
	--If all the resources match then nothing has changed
	--If none of the resources match then clear the local area
	--If only some of the resources don't match then one resource pocket is thumped out.
	--  for now we'll clear the local area, and can get more specific later if desired.
	
	if nearestScan ~= nil and 
	   nearestScan.distance <= INTERFACE_VARS.interface_DetectResourceChangesDistance then
	
		local compareResults = {};
		local compareCount = 0;
		local equivalentCount = 0;
		local nearestScanResources = SURV_RES.GetListOfResourcesByScanId(nearestScan.scanId, true);
		
		for i, resource in pairs(resources) do
			compareCount = compareCount + 1;
		end
		
		for i, nearResource in pairs(nearestScanResources) do
			equivalentCount = equivalentCount + 1;
		end
		
		if compareCount ~= equivalentCount then
			Debug.Log("Found discrepancies between this scan and the nearest scan. Cleaning area.")
			SURV_MARKER.CleanArea(0, INTERFACE_VARS.interface_ClearRadius, false);
			Component.GenerateEvent("MY_NOTIFY", {text = "Surveyor: "..Get_Localization_Value("FAILURE_RESOURCECHANGE")})
		else
			compareCount = 0;
			equivalentCount = 0;
			for i, nearResource in pairs(nearestScanResources) do
				compareResults[nearResource.itemTypeId] = {found = false, resource_quality = nearResource.resource_quality};
				compareCount = compareCount + 1;
				
				for i, resource in pairs(resources) do
					if nearResource.itemTypeId == resource.itemTypeId and
					   nearResource.resource_quality == resource.resource_quality then
						compareResults[nearResource.itemTypeId] = {found = true, resource_quality = nearResource.resource_quality};
						equivalentCount = equivalentCount + 1;
					end
				end
			end
			
			if compareCount ~= equivalentCount then
				Debug.Log("Found discrepancies between this scan and the nearest scan. Cleaning area.")
				SURV_MARKER.CleanArea(0, INTERFACE_VARS.interface_ClearRadius, false);
				Component.GenerateEvent("MY_NOTIFY", {text = "Surveyor: "..Get_Localization_Value("FAILURE_RESOURCECHANGE")})
			end		
		end
	end
end


----------------------------------------------------------
--nearbyMarkers[markerId] = {scanId, distance}
local function DetermineNearestScan()
	local nearbyMarkers = {};
	nearbyMarkers = SURV_MARKER.GetNearbyMarkers();
	nearestScan = nil;
	
	--Debug.Table("Nearby markers are", nearbyMarkers);
	--Debug.Table("Nearest scan starts as ", nearestScan);
	
	for markerId, item in pairs(nearbyMarkers) do
		if nearestScan == nil then
			nearestScan = {};
			nearestScan.scanId = item.scanId;
			nearestScan.distance = item.distance;
			--Debug.Table("Setting default nearest scan ", nearestScan);
		end
	
		--Debug.Log("Comparing ", nearestScan.distance, " versus ", item.distance);
		if nearestScan.distance > item.distance then
			nearestScan.scanId = item.scanId;
			nearestScan.distance = item.distance;
		end
	end	
	--Debug.Table("Determine nearest scan is", nearestScan);
end

----------------------------------------------------------
SURV_RES.CleanFailedArea = function(args)
	if INTERFACE_VARS.interface_DetectResourceChanges == true then
		DetermineNearestScan();
		
		if nearestScan ~= nil and 
	   	   nearestScan.distance <= INTERFACE_VARS.interface_ClearRadius then
	   		Component.GenerateEvent("MY_NOTIFY", {text = "Surveyor: "..Get_Localization_Value("FAILURE_RESOURCECHANGE")})
	   		SURV_MARKER.CleanArea(0, INTERFACE_VARS.interface_ClearRadius, false);
	   	end
	end
end
	
----------------------------------------------------------
SURV_RES.IsScanStillABestScan = function(resources)
	local stillValid = false;
	for i, resource in pairs(resources) do
		if resource.bestFlag == true then
			stillValid = true;
		end
	end
	return stillValid;
end

----------------------------------------------------------
local function IsThisABestScan(thisResource)
	--Debug.Log("      Is This a Best Scan?", thisResource.scanId);
	--Debug.Table("      Resource", thisResource);
	local bestScan = true;
	
	if resourceList == {} then
		--Debug.Log("             No resources found so far, so yes this is a best scan.");
		return bestScan;
	end
	
	local distanceResult = {isInCircle = false, distance = 0};
	local centerX = thisResource.coords.x;
	local centerY = thisResource.coords.y;
	
	--Get only the resources we want to compare
	local singleResourceList = SURV_RES.GetListOfResourceType(thisResource.itemTypeId, thisResource.resource_quality, true)

	--Loop through the existing resource list to find matching resources
	
	local compareCount = 0;
	nearestScan = {};
	
	for i, resource in pairs(singleResourceList) do
		   
		distanceResult = SURV_UTIL.IsInsideCircle(centerX, centerY, INTERFACE_VARS.interface_bestScanDistance, resource.coords.x, resource.coords.y);
		--Debug.Log("            Was the resource inside the radius? "..tostring(distanceResult));
		
		--It is nearby.  	   
	   	if distanceResult.isInCircle == true then
		
			compareCount = compareCount + 1;
			
			--Since the resource list is sorted by percentage we can check as we go against
			--the current resource.  If we hit our limit and the resource isn't bigger then 
			--discard it.
			if thisResource.percent < resource.percent then
				--Debug.Log("            Resource percent is less that some other scan")
				bestScan = false;
			else
				bestScan = true;
			end	
			
			--if we're over the user's quantity limit and this resource's percent 
			--isn't greater than anything else so far then we're done here.
			if compareCount > INTERFACE_VARS.interface_bestScanQty and bestScan == false then
				--Debug.Log("            There were ", INTERFACE_VARS.interface_bestScanQty, " better scans than this one.");
				return bestScan;
			end			
			
			--if we're over the user's quantity limit and this resource's percent
			--is greater then we need to mark items that are no longer the best
			if compareCount >= INTERFACE_VARS.interface_bestScanQty and bestScan == true then
				--Debug.Log("            Marking existing resource that is no longer the best: ", resource.scanId, resource.itemTypeId);
				SURV_RES.MarkResourceBestFlag(resource.scanId, resource.itemTypeId, false);
				SURV_MARKER.ProcessMarker(resource.scanId, resource.itemTypeId);
			end		
		end  --check distance
	end  --resource list loop
	
	if compareCount < INTERFACE_VARS.interface_bestScanQty then
		--if there aren't enough stored scans then the current scan is always accepted
		bestScan = true;
	end

	return bestScan;
end



----------------------------------------------------------
local function BuildResource(thisScan, details)
	--Debug.Log("      Build Resource")
	local thisResource = {};
	
	thisResource.scanId = thisScan.scanId;
	thisResource.zoneId = thisScan.zoneId;
	thisResource.scanDate = thisScan.scanDate;
	thisResource.scanTime = thisScan.scanTime;
	thisResource.ownerId = thisScan.ownerId;
	thisResource.instanceId = thisScan.instanceId;
	thisResource.locked = thisScan.locked;
	thisResource.bestFlag = true;
	thisResource.coords = {};
	thisResource.coords = thisScan.coords;
	
	local itemDetails = Game.GetItemInfoByType(details.itemTypeId);
	--Debug.Table("         Composition details: ", itemDetails);

	thisResource.itemTypeId = tonumber(details.itemTypeId);  --It starts as an entity, but we want to store it as a number.
	thisResource.resource_quality = details.resource_quality;
	thisResource.qualityColor = LIB_ITEMS.GetResourceQualityColor(details.resource_quality);
	thisResource.percent = details.percent;
	thisResource.percentTwo = string.sub(tostring(details.percent * 100), 1, 5).."%";
	thisResource.percentZero = tostring(SURV_UTIL.round(details.percent * 100), 0).."%";

	thisResource.resource_color = itemDetails.resource_color;
	thisResource.name = itemDetails.name;	
	thisResource.web_icon = itemDetails.web_icon;
	
	return thisResource;
end


----------------------------------------------------------
SURV_RES.ProcessScan = function(args)
	--Debug.Log("   Process Scan: ")
	local thisScan = {};
	local thisScanDetails = {};
	local keepThisScan = false;
	
	thisScanDetails = Game.GetResourceScanInfo(args.scanId);
	thisScanDetails.scanId = args.scanId;
	
	--Debug.Log("      Scan results: "..tostring(thisScanDetails));

	--Now build a base scan structure 	
	thisScan.scanId = thisScanDetails.scanId;  --get the scanId
	thisScan.scanDate = System.GetDate("%Y%m%d");  --YYYYMMDD
	thisScan.scanTime = System.GetDate("%H%M");    --HHMM
	thisScan.ownerId = thisScanDetails.ownerId;
	thisScan.zoneId = USEFUL_VALUES.l_zoneId;
	thisScan.instanceId = USEFUL_VALUES.l_instanceId;
	thisScan.locked = thisScanDetails.locked;
		
	--We will store the coordinates using the world chunk system
	local chunkCoords = Game.WorldToChunkCoord(thisScanDetails.x, thisScanDetails.y, thisScanDetails.z);
	thisScan.coords = {};
	thisScan.coords.storeX = chunkCoords.x;
	thisScan.coords.storeY = chunkCoords.y;
	thisScan.coords.storeZ = chunkCoords.z;
	thisScan.coords.chunkX = chunkCoords.chunkX;
	thisScan.coords.chunkY = chunkCoords.chunkY;
	thisScan.coords.x = thisScanDetails.x;
	thisScan.coords.y = thisScanDetails.y;
	thisScan.coords.z = thisScanDetails.z;
	
	local tempResource = {};
	local tempResources = {};
	local isSiftedEarth = false;
	
	--Build the list of resources, and reject any sifted earth
	for idx, details in pairs(thisScanDetails.composition) do
		tempResource = {};
		tempResource = BuildResource(thisScan, details);
		
		--Debug.Table("      Build this resource", tempResource);
		isSiftedEarth = IsThisSiftedEarth(tempResource.itemTypeId);
		if isSiftedEarth == false then
			--Debug.Log("      This resource is not Sifted Earth ", tempResource.name);
			table.insert(tempResources, tempResource);
		end		 
	end
	
	----------------------------------------------------------
	--if at least one of the resources' quality is greater than our minimum then keep it
	for idx, resource in pairs(tempResources) do
		--Debug.Log("Comparing ", resource.resource_quality, " with ", INTERFACE_VARS.interface_MinQuality)
		if resource.resource_quality >= INTERFACE_VARS.interface_MinQuality then
			keepThisScan = true;
		end
	end
	
	----------------------------------------------------------
	--if at least one of the resources' percentage is greater than our minimum then keep it
	if keepThisScan == true then
		keepThisScan = false;
		for idx, resource in pairs(tempResources) do
			--Debug.Log("Comparing ", resource.percentTwo, " with ", INTERFACE_VARS.interface_MinPercent)
			if (resource.percent * 100) >= INTERFACE_VARS.interface_MinPercent then
				keepThisScan = true;
			end
		end
	else
		--resource quality check failed
		if INTERFACE_VARS.interface_AnnounceFailures == true then
			Debug.Log("Quality failure.");
			Component.GenerateEvent("MY_NOTIFY", {text = "Surveyor: "..Get_Localization_Value("FAILURE_QUALITY")})
		end
		return keepThisScan;
	end 	
	
	----------------------------------------------------------
	--Determine if at least one resource is the best for this area 
	--If at least one of the resources is the best then keep the whole scan
	if keepThisScan == true then
		keepThisScan = false;
		if INTERFACE_VARS.interface_bestScanOn == true then
			--Debug.Table("      Prior to checking keep best scans here is our list", tempResources);
			for idx, resource in pairs(tempResources) do
				if keepThisScan == false then
					keepThisScan = IsThisABestScan(resource);
					resource.bestFlag = keepThisScan;
				end
			end
		else
			keepThisScan = true;
		end
	else
		--resource percent check failed
		if INTERFACE_VARS.interface_AnnounceFailures == true then
			Debug.Log("Percent failure.");
			Component.GenerateEvent("MY_NOTIFY", {text = Get_Localization_Value("FAILURE_PERCENTAGE")})
		end
		return keepThisScan;
	end
	
	--Even if we didn't have a best scan let's see if the resources in this area have changed
	if INTERFACE_VARS.interface_DetectResourceChanges == true then
		if IsSameOwner(thisScan.ownerId) == true then
			DetermineNearestScan();
			CompareNearbyScans(tempResources);
		end
	end
	
	----------------------------------------------------------
	--if at least one of the resources made it through all the checks then we keep this scan
	if keepThisScan == true then
		for idx, resource in pairs(tempResources) do
			Debug.Log("         Keeping Resource ", resource.name);
			SURV_RES.StoreResource(resource);
		end
	else
		--best scan failed
		if INTERFACE_VARS.interface_AnnounceFailures == true then
			Debug.Log("Bestscan failure.");
			Component.GenerateEvent("MY_NOTIFY", {text = "Surveyor: "..Get_Localization_Value("FAILURE_BESTSCAN")})
			return keepThisScan;
		end
	end  
	
	return keepThisScan;
end

----------------------------------------------------------
SURV_RES.AgeOutResources = function()
	Debug.Log("Aging out old resources.");
	
	if INTERFACE_VARS.interface_AutoAgeScans == true then

		local diff = 0;
		local nowDate = System.GetDate("%Y%m%d");  --YYYYMMDD
		local nowTime = System.GetDate("%H%M")    --HHMM
		--log("The half life is "..tostring(scanAge_Half).." days and the final age days is "..tostring(interface_ScanAge));
		
		--Copy the scan list. Otherwise as we delete from scanList the index gets messed up
		local scroller = {};
		for j, x in pairs(resourceList) do scroller[j] = x end		
	
		for index, thisScan in pairs(scroller) do 

			if thisScan.scanDate ~= nil then
				diff = SURV_UTIL.DateFindHoursDiff(thisScan.scanDate, nowDate, thisScan.scanTime, nowTime);
				--Debug.Log("Date comparison now "..tostring(nowDate)..tostring(nowTime).." versus "..tostring(thisScan.scanDate)..tostring(thisScan.scanTime).." results in a difference of "..tostring(diff).." hours.");
				--Debug.Log("Your scan age is "..INTERFACE_VARS.interface_ScanAge.." hours.");
				if diff > INTERFACE_VARS.interface_ScanAge then
					--log("Dropping this scan due to age. "..tostring(thisScan.scanId));
					SURV_RES.DeleteScan(thisScan.scanId);		
				end
			end
		end
	end
end
