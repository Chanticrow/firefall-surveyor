--[[
SURVEYOR_MARKER

This is more or less the marker collection manager.  
Generates the base marker concept data required for marker generation.
Performs marker/scan clearing.
Shows/hides markers.  
]]

if SURV_MARKER then
	-- only include once
	return nil
end

SURV_MARKER = {};

local markerList = {};

local INTERFACE_VARS = {};
local USEFUL_VALUES = {};
local markersShown = true;
local const_markerColor = "#0099FF";
local markerObj = {};
local mineralFilterTable = {};
local nearbyMarkers = {};

----------------------------------------------------------
SURV_MARKER.SetInterfaceValues = function(inUSEFUL_VALUES, inINTERFACE_VARS, inMineralFilterTable)
	log("Marker Style - Start Marker SetInterfaceValues: "..inINTERFACE_VARS.interface_MarkerStyle);
	INTERFACE_VARS = inINTERFACE_VARS;
	USEFUL_VALUES = inUSEFUL_VALUES;
	mineralFilterTable = inMineralFilterTable;
	--Debug.Log("   Marker style is ", INTERFACE_VARS.interface_MarkerStyle);
	Debug.Log("   Marker format is ", INTERFACE_VARS.interface_TitleFormat);
	--Debug.Log("   Color by quality is ", INTERFACE_VARS.interface_ColorByQuality);
	--Debug.Log("   Fade with Distance is ",  INTERFACE_VARS.interface_FadeWithDistance);
	--Debug.Log("   Fade Distance is ", INTERFACE_VARS.interface_FadeDistance);
	--Debug.Log("   Using web icons is ", INTERFACE_VARS.interface_WebIcons);
	--Debug.Table("   Mineral Filter Table is ", mineralFilterTable);
	--Debug.Table("   Useful values are ", USEFUL_VALUES);
	
	if INTERFACE_VARS.interface_MarkerStyle == "CLASSIC" then
		require "./surveyor_marker_classic";
		Debug.Log("Setting marker to classic mode.");
	elseif INTERFACE_VARS.interface_MarkerStyle == 2 then
		require "./surveyor_marker_firefall";
		Debug.Log("Setting marker to FireFall mode.");
	elseif INTERFACE_VARS.interface_MarkerStyle == "CHANTICROW" then
		require "./surveyor_marker_new";
		Debug.Log("Setting marker to Chanticrow mode.");
	else
		require "./surveyor_marker_classic";
		Debug.Log("Defaulting marker to classic mode.");
	end
	
	SURV_SUBMARKER.SetInterfaceValues(inINTERFACE_VARS);
	log("Marker Style - End Marker SetInterfaceValues: "..INTERFACE_VARS.interface_MarkerStyle);
end

----------------------------------------------------------
SURV_MARKER.SetShownState = function(showFlag)
	markersShown = showFlag;
end

----------------------------------------------------------
local function StoreMarker(markerList, thisMarker, markerId)
	--Debug.Log("       Saving marker ", markerId);
	markerList[markerId] = thisMarker;
end

----------------------------------------------------------
local function RemoveMarker(markerId)
	if markerList[markerId] then
		SURV_SUBMARKER.DestroyMarker(markerList[markerId].actualMarker);
		markerList[markerId] = nil;
	end
end

----------------------------------------------------------
SURV_MARKER.DeleteMarker = function(markerId) 
	RemoveMarker(markerId);
end

----------------------------------------------------------
SURV_MARKER.DeleteAllMarkers = function()
	
	for markerId, marker in pairs(markerList) do
		RemoveMarker(markerId);
	end
	
	markerList = {};
end

----------------------------------------------------------
SURV_MARKER.GetMarker = function(markerId) 
	if markerList[markerId] ~= nil then
		return markerList[markerId];
	else
		return nil;
	end
end

----------------------------------------------------------
SURV_MARKER.GetNearbyMarkers = function() 
	return nearbyMarkers;
end

----------------------------------------------------------
local function CheckMineralFilter(resourceList)
	--If any of the resources are not filtered then show the whole scan
	for i, resource in pairs(resourceList) do
		--Debug.Table("Checking resource filtering", resource);
		if mineralFilterTable[resource.itemTypeId] == true then
			return true;
		end
	end
	
	return false;
end

----------------------------------------------------------
local function CheckInstance(instanceId)
	--Do not show markers recorded from squad mates in other instances
	--Debug.Log("Checking instance id ", instanceId, " versus ", USEFUL_VALUES.l_instanceId)
	if instanceId == USEFUL_VALUES.l_instanceId then
		return true;
	else
		return false;
	end
end

----------------------------------------------------------
local function CheckPvPFlag()
	--Debug.Log("Checking pvp flag ", USEFUL_VALUES.l_pvpFlag)
	if USEFUL_VALUES.l_pvpFlag == true then
		return false;
	else
		return true;
	end
end

----------------------------------------------------------
local function ShowMarker(markerId)

	local myMarker = markerList[markerId];
	local worldmapFlag = true;
	local radarFlag = true;
	local hudFlag = true;
	
	local showThis = true;
	
	if markersShown == false then
		showThis = false;
	end
	
	if showThis == true then
		showThis = CheckMineralFilter(myMarker.resources);
	end
	
	if showThis == true then
		showThis = CheckInstance(myMarker.instanceId);
	end
	
	if showThis == true then
		showThis = CheckPvPFlag();
	end
		
	if showThis == true then		
		if INTERFACE_VARS.interface_FadeWithDistance == true then
			local myPos = Player.GetPosition();
			local distanceResult = SURV_UTIL.IsInsideCircle(myPos.x, myPos.y, INTERFACE_VARS.interface_FadeDistance, myMarker.x, myMarker.y);
			if distanceResult.isInCircle == true then
				hudFlag = true;
				nearbyMarkers[myMarker.markerId] = {scanId = myMarker.scanId, distance = distanceResult.distance};
			else
				hudFlag = false;
			end
		else
			hudFlag = true;
		end	
	else
		worldmapFlag = false;
		radarFlag = false;
		hudFlag = false;
	end	

	SURV_SUBMARKER.ShowMarker(myMarker.actualMarker, worldmapFlag, radarFlag, hudFlag);
end

----------------------------------------------------------
local function HideMarker(markerId)

	local myMarker = markerList[markerId];
	local worldmapFlag = false;
	local radarFlag = false;
	local hudFlag = false;

	SURV_SUBMARKER.ShowMarker(myMarker.actualMarker, worldmapFlag, radarFlag, hudFlag);
end

----------------------------------------------------------
local function SetMarkerColor(quality)
	if INTERFACE_VARS.interface_ColorByQuality == true then
		return LIB_ITEMS.GetResourceQualityColor(quality);
	else
		return const_markerColor;
	end
end

----------------------------------------------------------
local function BuildMarkerText(resource)
	local displayText = "";
	local resourceName = "";
	
	--Debug.Log("Title Format ", INTERFACE_VARS.interface_TitleFormat);
	if INTERFACE_VARS.interface_TitleFormat == "FULL_NAME_FMT" then  			--Raw Biopolymer^CY256-12.34%
		displayText = resource.name..resource.resource_quality.."-"..resource.percentZero;
	elseif INTERFACE_VARS.interface_TitleFormat == "SHORT_NAME_FMT" then		--
		if string.sub(resource.name, 1, 3) == "Raw" then
			resourceName = string.sub(resource.name, 5, 8);
		else
			resourceName = string.sub(resource.name, 1, 4);
		end
		displayText = resourceName.."^CY"..resource.resource_quality.."-"..resource.percentZero;
	end
	
	return displayText;
end

----------------------------------------------------------
SURV_MARKER.ProcessMarker = function(scanId)
	--Debug.Log("      Processing Marker", scanId)
	log("Marker Style - Marker ProcessMarker: "..INTERFACE_VARS.interface_MarkerStyle);
	local thisMarker = {};
	local tempResource = {};
	local markerResources = {};
	local totalResourcePercent = 0;
	local bestQuality = 0;
	local bestPercent = 0;
	local qualityColor = "";
	local stillValid = false;    --used for checking the bestFlag
	
	thisMarker.markerId = "surveyor_"..tostring(scanId);
	
	markerResources = SURV_RES.GetListOfResourcesByScanId(scanId, true);
	--Debug.Table("         Resources for this marker", markerResources);
	
	stillValid = SURV_RES.IsScanStillABestScan(markerResources);
	
	if stillValid == true then
	
		if markerList[thisMarker.markerId] ~= nil then
			RemoveMarker(thisMarker.markerId);
		end
	
		thisMarker.scanId = scanId;
		thisMarker.resources = {};
		
		for i, resource in pairs(markerResources) do
			thisMarker.x = resource.coords.x;
			thisMarker.y = resource.coords.y;
			thisMarker.z = resource.coords.z;
			thisMarker.scanDate = resource.scanDate;
			thisMarker.scanTime = resource.scanTime;
			thisMarker.ownerId = resource.ownerId;
			thisMarker.instanceId = resource.instanceId;	
			thisMarker.zoneId = resource.zoneId;
			totalResourcePercent = totalResourcePercent + resource.percent;
			tempResource = {};
			tempResource.itemTypeId = resource.itemTypeId;
			tempResource.name = resource.name;
			tempResource.percent = resource.percent;
			tempResource.resource_quality = resource.resource_quality;
			tempResource.qualityColor = resource.qualityColor;
			tempResource.display = BuildMarkerText(resource);
			tempResource.web_icon = resource.web_icon;
			tempResource.resource_color = resource.resource_color;
			
			if INTERFACE_VARS.interface_WebIconsSort == "WEBICONS_QUALITY" then
				if resource.resource_quality > bestQuality then
					bestQuality = resource.resource_quality;
					thisMarker.web_icon = resource.web_icon;
				end
			elseif INTERFACE_VARS.interface_WebIconsSort == "WEBICONS_QUANTITY" then
				if resource.percent > bestPercent then
					bestQuality = resource.resource_quality;
					bestPercent = resource.percent;
					thisMarker.web_icon = resource.web_icon;
				end			
			end
					
			table.insert(thisMarker.resources, tempResource);
		end
		
		--Debug.Table("         Marker so far: ", thisMarker);
		--Debug.Log("Owner id: ", thisMarker.ownerId);
		--Debug.Table("         Owner info", Game.GetTargetInfo(thisMarker.ownerId));
		if Game.GetTargetInfo(thisMarker.ownerId) ~= nil then
			thisMarker.ownerName = Game.GetTargetInfo(thisMarker.ownerId).name;
		else
			thisMarker.ownerName = "Unknown";
		end
		thisMarker.qualityColor = SetMarkerColor(bestQuality);
		thisMarker.totalPercent = totalResourcePercent;
		thisMarker.alpha = 1;
		
		--Debug.Log("Percent flag ", INTERFACE_VARS.interface_ShowTotalPercent);
		if INTERFACE_VARS.interface_ShowTotalPercent == true then
			tempResource = {};
			tempResource.name = "Total";
			tempResource.percent = thisMarker.totalPercent;
			tempResource.display = "Total: "..string.sub(tostring(totalResourcePercent * 100), 1, 5).."%";
			tempResource.resource_quality = "";
			--Debug.Table("Total resource ", tempResource);
			table.insert(thisMarker.resources, tempResource);
		end
		
			--Display date if scan aging is on, and fade markers on halflife
		if INTERFACE_VARS.interface_AutoAgeScans == true then
			if thisMarker.scanDate ~= nil then		
				local nowDate = System.GetDate("%Y%m%d");  --YYYYMMDD
				local nowTime = System.GetDate("%H%M")    --HHMM
				local diff = SURV_UTIL.DateFindHoursDiff(thisMarker.scanDate, nowDate, thisMarker.scanTime, nowTime);
				
				--Debug.Log("Comparing ", diff, " against ", INTERFACE_VARS.ScanAge_Half);
				if diff > INTERFACE_VARS.ScanAge_Half then
					thisMarker.alpha = 0.5;   --fade markers older than half the scan age
				end
			end
		end		
		
		--Debug.Table("         Marker", thisMarker);
		
		local actualMarker = {};
		local actualMapMarker = {};
		
		actualMarker = SURV_SUBMARKER.BuildMarker(thisMarker);
		
		thisMarker.actualMarker = actualMarker;
		
		StoreMarker(markerList, thisMarker, thisMarker.markerId);
		
		Callback2.FireAndForget(function() ShowMarker(thisMarker.markerId); end, nil, INTERFACE_VARS.interface_DefaultMarkerFadeTime);
	else
		for i, resource in pairs(markerResources) do
			SURV_RES.RemoveResourceFromScan(markerResources.scanId, markerResources.itemTypeId);
			RemoveMarker(thisMarker.markerId);
		end
	end
end

----------------------------------------------------------
SURV_MARKER.MarkersHideAll = function()
for markerId, marker in pairs(markerList) do
		--Debug.Log("      Hiding marker", markerId);
		HideMarker(markerId);
	end
end

----------------------------------------------------------
SURV_MARKER.MarkersShowAll = function()
	nearbyMarkers = {};
	for markerId, marker in pairs(markerList) do
		--Debug.Log("      Showing marker", markerId);
		ShowMarker(markerId);
	end
end

----------------------------------------------------------
SURV_MARKER.ShowHideToggle = function(hideFlag)
	--Debug.Log("   Markers Show Hide All ", hideFlag)
	if hideFlag ~= nil then
		SURV_MARKER.SetShownState(hideFlag);
	end
	
	if markersShown == true then
		SURV_MARKER.SetShownState(false);
		SURV_MARKER.MarkersHideAll();
	else
		SURV_MARKER.SetShownState(true);
		SURV_MARKER.MarkersShowAll();
	end
end


----------------------------------------------------------
SURV_MARKER.CleanArea = function(inMarkerId, clearRadius, selfRemove)
	--log("   Marker Clean Area "..tostring(markerId))
	local myMarker = {};
	local isInCircle = true;
	local myX = 0;
	local myY = 0;
	
	if inMarkerId == 0 then
		local myPosition = Player.GetPosition();
		myX = myPosition.x;
		myY = myPosition.y;
	else
		myMarker = markerList[inMarkerId];
		myX = myMarker.x;
		myY = myMarker.y;
	end

	--log("Clearing markers around this x/y: "..myX.."/"..myY);
	--Copy the markerlist. Otherwise as we delete from markerList the index gets messed up
	local scroller = {};
	for j, x in pairs(markerList) do scroller[j] = x end
	
	for markerId, marker in pairs(scroller) do
	
		if markerId == inMarkerId then
			if selfRemove == true then
				SURV_RES.DeleteScan(marker.scanId);
				SURV_SUBMARKER.DestroyMarker(marker.actualMarker);
				markerList[markerId] = nil;
			end
		else
			local distanceResult = SURV_UTIL.IsInsideCircle(myX, myY, tonumber(clearRadius), marker.x, marker.y)

			if distanceResult.isInCircle == true then
				SURV_RES.DeleteScan(marker.scanId);
				SURV_SUBMARKER.DestroyMarker(marker.actualMarker);
				markerList[markerId] = nil;
			end
		end
	end
end